#include "pch.h"
#include "GameMatrix3.h"

GameMatrix3::GameMatrix3()
{
}

GameMatrix3::GameMatrix3(const GameMatrix3 & matrix)
{
	row00.x = matrix.row00.x;
	row00.y = matrix.row00.y;
	row00.z = matrix.row00.z;

	row01.x = matrix.row01.x;
	row01.y = matrix.row01.y;
	row01.z = matrix.row01.z;

	row02.x = matrix.row02.x;
	row02.y = matrix.row02.y;
	row02.z = matrix.row02.z;
}

GameMatrix3::GameMatrix3(GameVector3 & row00, GameVector3 & row01, GameVector3 & row02)
{
	this->row00.x = row00.x;
	this->row00.y = row00.y;
	this->row00.z = row00.z;

	this->row01.x = row01.x;
	this->row01.y = row01.y;
	this->row01.z = row01.z;

	this->row02.x = row02.x;
	this->row02.y = row02.y;
	this->row02.z = row02.z;
}

GameMatrix3::~GameMatrix3()
{
}

GameVector3 & GameMatrix3::operator[](size_t index)
{
	switch (index)
	{
		case 0:
			return row00;

		case 1:
			return row01;

		case 2:
			return row02;

		default:
			throw new LogicErrorException("Invalid Index");
	}
}

GameMatrix3 GameMatrix3::operator+(const GameMatrix3 & matrix)
{
	GameMatrix3 result(matrix);

	result.row00.x += this->row00.x;
	result.row00.y += this->row00.y;
	result.row00.z += this->row00.z;

	result.row01.x += this->row01.x;
	result.row01.y += this->row01.y;
	result.row01.z += this->row01.z;

	result.row02.x += this->row02.x;
	result.row02.y += this->row02.y;
	result.row02.z += this->row02.z;

	return result;
}

GameMatrix3 GameMatrix3::operator*(const GameMatrix3 & matrix)
{
	GameMatrix3 result;

	result.row00.x = this->row00.x * matrix.row00.x + this->row00.y * matrix.row01.x + this->row00.z * matrix.row02.x;
	result.row00.y = this->row00.x * matrix.row00.y + this->row00.y * matrix.row01.y + this->row00.z * matrix.row02.y;
	result.row00.z = this->row00.x * matrix.row00.z + this->row00.y * matrix.row01.z + this->row00.z * matrix.row02.z;
	result.row01.x = this->row01.x * matrix.row00.x + this->row01.y * matrix.row01.x + this->row01.z * matrix.row02.x;
	result.row01.y = this->row01.x * matrix.row00.y + this->row01.y * matrix.row01.y + this->row01.z * matrix.row02.y;
	result.row01.z = this->row01.x * matrix.row00.z + this->row01.y * matrix.row01.z + this->row01.z * matrix.row02.z;
	result.row02.x = this->row02.x * matrix.row00.x + this->row02.y * matrix.row01.x + this->row02.z * matrix.row02.x;
	result.row02.y = this->row02.x * matrix.row00.y + this->row02.y * matrix.row01.y + this->row02.z * matrix.row02.y;
	result.row02.z = this->row02.x * matrix.row00.z + this->row02.y * matrix.row01.z + this->row02.z * matrix.row02.z;

	return result;
}

void GameMatrix3::transpose()
{
	GameMatrix3 transposed = GameMatrix3::transpose(*this);
	this->row00.x = transposed.row00.x;
	this->row00.y = transposed.row00.y;
	this->row00.z = transposed.row00.z;

	this->row01.x = transposed.row01.x;
	this->row01.y = transposed.row01.y;
	this->row01.z = transposed.row01.z;

	this->row02.x = transposed.row02.x;
	this->row02.y = transposed.row02.y;
	this->row02.z = transposed.row02.z;
}

GameMatrix3 GameMatrix3::transpose(GameMatrix3 & matrix)
{
	GameMatrix3 result;

	result[0].x = matrix[0].x;
	result[0].y = matrix[1].x;
	result[0].z = matrix[2].x;

	result[1].x = matrix[0].y;
	result[1].y = matrix[1].y;
	result[1].z = matrix[2].y;

	result[2].x = matrix[0].z;
	result[2].y = matrix[1].z;
	result[2].z = matrix[2].z;

	return result;
}

GameMatrix3 operator*(float scalar, const GameMatrix3 & matrix)
{
	GameMatrix3 result(matrix);

	result.row00.x *= scalar;
	result.row00.y *= scalar;
	result.row00.z *= scalar;

	result.row01.x *= scalar;
	result.row01.y *= scalar;
	result.row01.z *= scalar;

	result.row02.x *= scalar;
	result.row02.y *= scalar;
	result.row02.z *= scalar;

	return result;
}

GameMatrix3 operator*(const GameMatrix3 & matrix, float scalar)
{
	return scalar * matrix;
}
