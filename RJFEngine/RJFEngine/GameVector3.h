#pragma once

class GameVector3
{
	friend GameVector3 & operator * (float scalar, const GameVector3 & vector3);
	friend GameVector3 & operator * (const GameVector3 & vector3, float scalar);
	friend GameVector3 & operator / (float scalar, const GameVector3 & vector3);
	friend GameVector3 & operator / (const GameVector3 & vector3, float scalar);

	public:
		float x, y, z;

		float& operator[](const unsigned int index);
		GameVector3 & operator + (const GameVector3 & vector3);
		GameVector3 & operator * (const GameVector3 & vector);

		GameVector3();
		GameVector3(const GameVector3 & vector3);
		GameVector3(float x, float y);
		GameVector3(float valueX,float valueY,float valueZ);
		~GameVector3();

		void invert();
		float getMagnitude();
		float dotProduct(const GameVector3 & vector);
};

