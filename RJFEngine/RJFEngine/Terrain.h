#pragma once
#include <vector>

using std::vector;

class Terrain
{
	protected:
		XMVECTORF32 color;
		int width;
		int height;
		int vertexCount;
		int indexCount;
		Vector3 * vertexList;
		unsigned short * indices;
		Vector3 * heightMap;

		bool loadHeightMap(std::string fileName);
		void normalizeHeightMap();

	public:
		static const int MAX_DETAIL = 73;

		Terrain();
		Terrain(int width, int height);
		Terrain(const Terrain &);
		~Terrain();

		void init();
		void draw(Graphics * graphics);
};

