#pragma once
#include "DeviceResources.h"

using DX::DeviceResources;

class ResourceManager
{
	private:
		// Singleton
		static ResourceManager * instance;

		// Device resources
		static DeviceResources * deviceResources;

		// Effects
		std::unique_ptr<EffectFactory> fxFactory;
		std::shared_ptr<IEffect> fxPrimitive;
		std::shared_ptr<IEffect> fxLine;

		// Common States
		std::unique_ptr<CommonStates> commonStates;

		ResourceManager();
		
		void createCommonStates();
		void createEffectFactory();
		void createPrimitiveFX();
		void createLineFX();

	public:
		~ResourceManager();

		static void createInstance(DeviceResources * deviceResources);
		static BasicEffect * getFXPrimitive();
		static BasicEffect * getFXLine();
		static GameObject * InstantiateGameObject(string name, Quaternion rotation, Vector3 position, bool staticObject, vector<GameObject *> * objects);
		static void updateView(Matrix view);
		static void updateProjection(Matrix projection);
};

