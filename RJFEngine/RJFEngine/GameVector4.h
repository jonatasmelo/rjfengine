#pragma once
#include "GameVector3.h"

class GameVector4 : public GameVector3
{
	friend GameVector4 & operator * (float scalar, const GameVector4 & vector4);
	friend GameVector4 & operator * (const GameVector4 & vector4, float scalar);
	friend GameVector4 & operator / (float scalar, const GameVector4 & vector4);
	friend GameVector4 & operator / (const GameVector4 & vector4, float scalar);

	public:
		float w;

		float& operator[](const unsigned int index);
		GameVector4 & operator + (const GameVector4 & vector4);
		GameVector4 & operator * (const GameVector4 & vector);

		GameVector4();
		GameVector4(const GameVector4 & vector4);
		GameVector4(float x, float y);
		GameVector4(float x, float y, float z);
		GameVector4(float valueX, float valueY, float valueZ, float valueW);
		~GameVector4();
};

