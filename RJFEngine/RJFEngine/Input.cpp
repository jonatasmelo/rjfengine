#include "pch.h"
#include "Input.h"

Input* Input::instance = nullptr;
Input::MouseButton* Input::mouseLeftButton = nullptr;
Input::MouseButton* Input::mouseMiddleButton = nullptr;
Input::MouseButton* Input::mouseRightButton = nullptr;

Input::Input()
{
	this->mouseLeftButton = new MouseButton(MouseKey::LEFT_BUTTON);
	this->mouseMiddleButton = new MouseButton(MouseKey::MIDDLE_BUTTON);
	this->mouseRightButton = new MouseButton(MouseKey::RIGHT_BUTTON);
}

//Static Method
Input* Input::getInstance()
{
	//static Input* instance = new Input();
	if (instance == nullptr) {
		instance = new Input();
	}

	return instance;
}

void Input::setWindowReference(HWND windowRef)
{
	mouseDevice = std::make_unique<Mouse>();
	mouseDevice->SetWindow(windowRef);

	keyboardDevice = std::make_unique<Keyboard>();
}

//Static Method
bool Input::getMouseButtonDown(short mouseButton)
{
	Mouse::ButtonStateTracker tracker;
	Mouse::State state = getInstance()->mouseDevice->GetState();
	tracker.Update(state);

	if (mouseButton == MouseKey::LEFT_BUTTON)
	{
		if (tracker.leftButton == ButtonState::PRESSED)
		{
			if (mouseLeftButton->pressed)
			{
				return !mouseLeftButton->pressed;
			}

			return mouseLeftButton->pressed = true;
		}
		if (tracker.leftButton == ButtonState::UP)
		{
			mouseLeftButton->pressed = false;
		}
	}

	if (mouseButton == MouseKey::MIDDLE_BUTTON)
	{
		if (tracker.middleButton == ButtonState::PRESSED)
		{
			if (mouseMiddleButton->pressed)
			{
				return !mouseMiddleButton->pressed;
			}

			return mouseMiddleButton->pressed = true;
		}
		if (tracker.middleButton == ButtonState::UP)
		{
			mouseMiddleButton->pressed = false;
		}
	}

	if (mouseButton == MouseKey::RIGHT_BUTTON)
	{
		if (tracker.rightButton == ButtonState::PRESSED)
		{
			if (mouseRightButton->pressed)
			{
				return !mouseRightButton->pressed;
			}

			return mouseRightButton->pressed = true;
		}
		if (tracker.rightButton == ButtonState::UP)
		{
			mouseRightButton->pressed = false;
		}
	}

	return false;
}

bool Input::getMouseButtonUp(short mouseButton)
{
	Mouse::ButtonStateTracker tracker;
	Mouse::State state = getInstance()->mouseDevice->GetState();
	tracker.Update(state);

	if (mouseButton == MouseKey::LEFT_BUTTON)
	{
		if (tracker.leftButton == ButtonState::PRESSED && !mouseLeftButton->pressed)
		{
			mouseLeftButton->pressed = true;
		}
		if (tracker.leftButton == ButtonState::UP && mouseLeftButton->pressed)
		{
			mouseLeftButton->pressed = false;
			return true;
		}
	}

	if (mouseButton == MouseKey::MIDDLE_BUTTON)
	{
		if (tracker.middleButton == ButtonState::PRESSED && !mouseMiddleButton->pressed)
		{
			mouseMiddleButton->pressed = true;
		}
		if (tracker.middleButton == ButtonState::UP && mouseMiddleButton->pressed)
		{
			mouseMiddleButton->pressed = false;
			return true;
		}
	}

	if (mouseButton == MouseKey::RIGHT_BUTTON)
	{
		if (tracker.rightButton == ButtonState::PRESSED && !mouseRightButton->pressed)
		{
			mouseRightButton->pressed = true;
		}
		if (tracker.rightButton == ButtonState::UP && mouseRightButton->pressed)
		{
			mouseRightButton->pressed = false;
			return true;
		}
	}

	return false;
}

bool Input::getMouseButtonPressed(short mouseButton)
{
	Mouse::ButtonStateTracker tracker;
	Mouse::State state = instance->mouseDevice->GetState();
	tracker.Update(state);

	if (mouseButton == MouseKey::LEFT_BUTTON)
	{
		return tracker.leftButton == ButtonState::PRESSED;
	}

	if (mouseButton == MouseKey::MIDDLE_BUTTON)
	{
		return tracker.middleButton == ButtonState::PRESSED;
	}

	if (mouseButton == MouseKey::RIGHT_BUTTON)
	{
		return tracker.rightButton == ButtonState::PRESSED;
	}

	return false;
}

Vector3 Input::getMousePosition()
{
	return Vector3(instance->mouseDevice->GetState().x, instance->mouseDevice->GetState().y, 0.f);
}

MouseMode Input::getMouseMode()
{
	Mouse::Mode mode = instance->mouseDevice->GetState().positionMode;
	return (Mouse::MODE_ABSOLUTE == mode ? MouseMode::ABSOLUTE : MouseMode::RELATIVE);
}

void Input::setMouseMode(MouseMode newMode)
{
	instance->mouseDevice->SetMode(newMode == MouseMode::ABSOLUTE ? Mouse::MODE_ABSOLUTE : Mouse::MODE_RELATIVE);
}

bool Input::getKeyDown(Keyboard::Keys keycode)
{
	Keyboard::KeyboardStateTracker tracker;
	Keyboard::State state = instance->keyboardDevice->GetState();
	tracker.Update(state);

	return tracker.IsKeyPressed(keycode);
}

bool Input::getKeyUp(Keyboard::Keys keycode) {
	Keyboard::KeyboardStateTracker tracker;
	Keyboard::State state = instance->keyboardDevice->GetState();
	tracker.Update(state);

	return !tracker.IsKeyPressed(keycode);
}