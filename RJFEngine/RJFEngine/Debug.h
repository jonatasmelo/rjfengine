#pragma once

class Debug
{

private: 
	static Debug* instance;
	Debug();

protected:
	Graphics* graphics = nullptr;
	XMVECTORF32 color = Colors::GreenYellow;

public:	
	static Debug* GetInstance();
	
	static void SetColor(XMVECTORF32 color);

	static XMVECTORF32 GetColor();
	
	static void SetGraphics(Graphics* graphics);

	static void DrawCollider(Collider* collider);
};