#include "pch.h"
#include "SceneHandler.h"

SceneHandler::SceneHandler()
{
	state = GameState::MENU;
	menu = std::make_unique<Scene>();
	currentScene = std::make_unique<Scene>();
}

SceneHandler::SceneHandler(Scene * menu)
{
	if (menu != nullptr) {
		this->menu = std::unique_ptr<Scene>(menu);
	}

	state = GameState::MENU;
}

SceneHandler::SceneHandler(Scene * startScene, Scene * menu)
{
	if (startScene != nullptr) {
		this->currentScene = std::unique_ptr<Scene>(startScene);
	}

	if (menu != nullptr) {
		this->menu = std::unique_ptr<Scene>(menu);
	}

	state = GameState::PLAYING;
}

SceneHandler::~SceneHandler()
{
	resetResources();
}

void SceneHandler::changeScene(Scene * newScene)
{
	currentScene.reset();

	if (newScene != nullptr) {
		newScene->initDeviceDependentResources();
		newScene->initWindowSizeDependentResources();
		newScene->setCamera(camera.get());
		currentScene = std::unique_ptr<Scene>(newScene);
	}
}

void SceneHandler::initDeviceDependentResources()
{
	if (menu.get() != nullptr) {
		menu->initDeviceDependentResources();
	}

	if (currentScene.get() != nullptr) {
		currentScene->initDeviceDependentResources();
	}

	CreateMaterial(L"MetalTexture.DDS", true);
}

void SceneHandler::initWindowSizeDependentResources()
{
	if (menu.get() != nullptr) {
		menu->initWindowSizeDependentResources();
		menu->setCamera(camera.get());
	}

	if (currentScene.get() != nullptr) {
		currentScene->initWindowSizeDependentResources();
		currentScene->setCamera(camera.get());
	}
}

void SceneHandler::updatePhase(DX::StepTimer const & timer)
{
	switch (state)
	{
	case GameState::PLAYING:
			if (!currentScene->update(timer)) {
				state = GameState::MENU;
				changeScene(nullptr);
			}
			break;

		case GameState::PAUSED:
			break;

		case GameState::MENU:
			menu->update(timer);
			break;

		default:
			// Fix the Game State
			state = GameState::MENU;
			break;
	}

	graphics->updateViewMatrix();
	graphics->updateProjectionMatrix();
}

void SceneHandler::renderPhase()
{
	Scene * newScene = nullptr;
	
	switch (state)
	{
		case GameState::PLAYING:
			newScene = currentScene->render(graphics.get());
			break;

		case GameState::PAUSED:
			break;

		case GameState::MENU:
			newScene = menu->render(graphics.get());
			break;

		default:
			// Fix the Game State
			state = GameState::MENU;
			break;
	}

	if (newScene != nullptr) {
		changeScene(newScene);
		state = GameState::PLAYING;
	}
	else {
		state = GameState::MENU;
	}
}

void SceneHandler::resetResources()
{
	menu->reset();
	menu.reset();

	currentScene->reset();
	currentScene.reset();
}

vector<GameObject*>* SceneHandler::getObjectList()
{
	switch (state)
	{
		case GameState::PLAYING:
			return currentScene->getObjectList();

		case GameState::PAUSED:
			break;

		case GameState::MENU:
			return menu->getObjectList();

		default:
			break;
	}

	return nullptr;
}
