#include "pch.h"
#include "GameMatrix4.h"

GameMatrix4::GameMatrix4()
{
}

GameMatrix4::GameMatrix4(const GameMatrix4 & matrix)
{
	row00.x = matrix.row00.x;
	row00.y = matrix.row00.y;
	row00.z = matrix.row00.z;
	row00.w = matrix.row00.w;

	row01.x = matrix.row01.x;
	row01.y = matrix.row01.y;
	row01.z = matrix.row01.z;
	row01.w = matrix.row01.w;

	row02.x = matrix.row02.x;
	row02.y = matrix.row02.y;
	row02.z = matrix.row02.z;
	row02.w = matrix.row02.w;

	row03.x = matrix.row03.x;
	row03.y = matrix.row03.y;
	row03.z = matrix.row03.z;
	row03.w = matrix.row03.w;
}

GameMatrix4::GameMatrix4(GameVector4 & row00, GameVector4 & row01, GameVector4 & row02)
{
	this->row00.x = row00.x;
	this->row00.y = row00.y;
	this->row00.z = row00.z;
	this->row00.w = row00.w;

	this->row01.x = row01.x;
	this->row01.y = row01.y;
	this->row01.z = row01.z;
	this->row01.w = row01.w;

	this->row02.x = row02.x;
	this->row02.y = row02.y;
	this->row02.z = row02.z;
	this->row02.w = row02.w;

	this->row03.x = 0;
	this->row03.y = 0;
	this->row03.z = 0;
	this->row03.w = 1;
}

GameMatrix4::GameMatrix4(GameVector4 & row00, GameVector4 & row01, GameVector4 & row02, GameVector4 & row03)
{
	this->row00.x = row00.x;
	this->row00.y = row00.y;
	this->row00.z = row00.z;
	this->row00.w = row00.w;

	this->row01.x = row01.x;
	this->row01.y = row01.y;
	this->row01.z = row01.z;
	this->row01.w = row01.w;

	this->row02.x = row02.x;
	this->row02.y = row02.y;
	this->row02.z = row02.z;
	this->row02.w = row02.w;

	this->row03.x = row03.x;
	this->row03.y = row03.y;
	this->row03.z = row03.z;
	this->row03.w = row03.w;
}

GameMatrix4::~GameMatrix4()
{
}

GameVector4 & GameMatrix4::operator[](size_t index)
{
	switch (index)
	{
		case 0:
			return row00;

		case 1:
			return row01;

		case 2:
			return row02;

		case 3:
			return row03;

		default:
			throw new LogicErrorException("Invalid Index");
	}
}

GameMatrix4 GameMatrix4::operator+(const GameMatrix4 & matrix)
{
	GameMatrix4 result(matrix);

	result.row00.x += this->row00.x;
	result.row00.y += this->row00.y;
	result.row00.z += this->row00.z;
	result.row00.w += this->row00.w;

	result.row01.x += this->row01.x;
	result.row01.y += this->row01.y;
	result.row01.z += this->row01.z;
	result.row01.w += this->row01.w;

	result.row02.x += this->row02.x;
	result.row02.y += this->row02.y;
	result.row02.z += this->row02.z;
	result.row02.w += this->row02.w;

	result.row03.x += this->row03.x;
	result.row03.y += this->row03.y;
	result.row03.z += this->row03.z;
	result.row03.w += this->row03.w;

	return result;
}

GameMatrix4 GameMatrix4::operator*(const GameMatrix4 & matrix)
{
	GameMatrix4 result;

	result.row00.x = this->row00.x * matrix.row00.x + this->row00.y * matrix.row01.x + this->row00.z * matrix.row02.x + this->row00.w * matrix.row03.x;
	result.row00.y = this->row00.x * matrix.row00.y + this->row00.y * matrix.row01.y + this->row00.z * matrix.row02.y + this->row00.w * matrix.row03.y;
	result.row00.z = this->row00.x * matrix.row00.z + this->row00.y * matrix.row01.z + this->row00.z * matrix.row02.z + this->row00.w * matrix.row03.z;
	result.row00.w = this->row00.x * matrix.row00.w + this->row00.y * matrix.row01.w + this->row00.z * matrix.row02.w + this->row00.w * matrix.row03.w;
	
	result.row01.x = this->row01.x * matrix.row00.x + this->row01.y * matrix.row01.x + this->row01.z * matrix.row02.x + this->row00.w * matrix.row03.x;
	result.row01.y = this->row01.x * matrix.row00.y + this->row01.y * matrix.row01.y + this->row01.z * matrix.row02.y + this->row00.w * matrix.row03.y;
	result.row01.z = this->row01.x * matrix.row00.z + this->row01.y * matrix.row01.z + this->row01.z * matrix.row02.z + this->row00.w * matrix.row03.z;
	result.row01.z = this->row01.x * matrix.row00.w + this->row01.y * matrix.row01.w + this->row01.z * matrix.row02.w + this->row00.w * matrix.row03.w;
	
	result.row02.x = this->row02.x * matrix.row00.x + this->row02.y * matrix.row01.x + this->row02.z * matrix.row02.x + this->row00.w * matrix.row03.x;
	result.row02.y = this->row02.x * matrix.row00.y + this->row02.y * matrix.row01.y + this->row02.z * matrix.row02.y + this->row00.w * matrix.row03.y;
	result.row02.z = this->row02.x * matrix.row00.z + this->row02.y * matrix.row01.z + this->row02.z * matrix.row02.z + this->row00.w * matrix.row03.z;
	result.row02.z = this->row02.x * matrix.row00.w + this->row02.y * matrix.row01.w + this->row02.z * matrix.row02.w + this->row00.w * matrix.row03.w;
	
	result.row03.x = this->row03.x * matrix.row00.x + this->row03.y * matrix.row01.x + this->row03.z * matrix.row02.x + this->row03.w * matrix.row03.x;
	result.row03.y = this->row03.x * matrix.row00.y + this->row03.y * matrix.row01.y + this->row03.z * matrix.row02.y + this->row03.w * matrix.row03.y;
	result.row03.z = this->row03.x * matrix.row00.z + this->row03.y * matrix.row01.z + this->row03.z * matrix.row02.z + this->row03.w * matrix.row03.z;
	result.row03.w = this->row03.x * matrix.row00.w + this->row03.y * matrix.row01.w + this->row03.z * matrix.row02.w + this->row03.w * matrix.row03.w;

	return result;
}

void GameMatrix4::transpose()
{
	GameMatrix4 transposed = GameMatrix4::transpose(*this);
	this->row00.x = transposed.row00.x;
	this->row00.y = transposed.row00.y;
	this->row00.z = transposed.row00.z;
	this->row00.w = transposed.row00.w;

	this->row01.x = transposed.row01.x;
	this->row01.y = transposed.row01.y;
	this->row01.z = transposed.row01.z;
	this->row01.w = transposed.row01.w;

	this->row02.x = transposed.row02.x;
	this->row02.y = transposed.row02.y;
	this->row02.z = transposed.row02.z;
	this->row02.w = transposed.row02.w;
}

GameMatrix4 GameMatrix4::transpose(GameMatrix4 & matrix)
{
	GameMatrix4 result;

	result[0].x = matrix[0].x;
	result[0].y = matrix[1].x;
	result[0].z = matrix[2].x;
	result[0].w = matrix[3].x;

	result[1].x = matrix[0].y;
	result[1].y = matrix[1].y;
	result[1].z = matrix[2].y;
	result[1].w = matrix[3].y;

	result[2].x = matrix[0].z;
	result[2].y = matrix[1].z;
	result[2].z = matrix[2].z;
	result[2].w = matrix[3].z;

	result[3].x = matrix[0].w;
	result[3].y = matrix[1].w;
	result[3].z = matrix[2].w;
	result[3].w = matrix[3].w;

	return result;
}

GameMatrix4 operator*(float scalar, const GameMatrix4 & matrix)
{
	GameMatrix4 result(matrix);

	result.row00.x *= scalar;
	result.row00.y *= scalar;
	result.row00.z *= scalar;
	result.row00.w *= scalar;

	result.row01.x *= scalar;
	result.row01.y *= scalar;
	result.row01.z *= scalar;
	result.row01.w *= scalar;

	result.row02.x *= scalar;
	result.row02.y *= scalar;
	result.row02.z *= scalar;
	result.row02.w *= scalar;

	return result;
}

GameMatrix4 operator*(const GameMatrix4 & matrix, float scalar)
{
	return scalar * matrix;
}
