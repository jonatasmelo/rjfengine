#pragma once

enum MouseKey { LEFT_BUTTON = 0, MIDDLE_BUTTON = 1, RIGHT_BUTTON = 2 };
enum ButtonState { UP = 0, HELD = 1, RELEASED = 2, PRESSED = 3 };
enum MouseMode { ABSOLUTE, RELATIVE };

class Input
{
	public:
		class MouseButton {
			public:
				short code;
				bool pressed;

				MouseButton(short code) {
					this->code = code;
				}
		};

		static Input* getInstance();
		static bool getMouseButtonDown(short mouseButton);
		static bool getMouseButtonUp(short mouseButton);
		static bool getMouseButtonPressed(short mouseButton);

		static bool getKeyDown(Keyboard::Keys keycode);
		static bool getKeyUp(Keyboard::Keys keycode);

		static Vector3 getMousePosition();
		static MouseMode getMouseMode();
		static void setMouseMode(MouseMode newMode);

		void setWindowReference(HWND windowRef);

	private:
		static Input * instance;
		std::unique_ptr<Mouse> mouseDevice;
		std::unique_ptr<Keyboard> keyboardDevice;

		static MouseButton* mouseLeftButton;
		static MouseButton* mouseMiddleButton;
		static MouseButton* mouseRightButton;

		Input();
};

