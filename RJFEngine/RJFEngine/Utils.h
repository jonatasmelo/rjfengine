#pragma once
#include "pch.h"
#include "Scene.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

// Generic Enums
enum Axis { X, Y, Z, W };
enum Dimensions { ONE_DIMENSION = 1, TWO_DIMENSIONS = 2, THREE_DIMENSIONS = 3, FOUR_DIMENSIONS = 4 };
enum GameState { PLAYING, PAUSED, SPLASH_SCREEN, MENU };

// MathEngine Enums
enum CustomLine { START_POINT, END_POINT };
enum CustomRectangle { UPPER_LEFT, UPPER_RIGHT, LOWER_RIGHT, LOWER_LEFT };
enum CustomTriangle { TOP, LEFT, RIGHT };
enum CustomCircle { CENTER };

class GameUtils
{
	public:
		static const std::wstring WINDOW_TITLE;
		static const int FPS;
		// The minimum size is 320x200
		static const int DEFAULT_WIDTH;
		static const int DEFAULT_HEIGHT;

		static const XMVECTORF32 DEFAULT_BACKGROUND_COLOR;
		static const float UNIT_SCALE;

		static const Vector3 DEFAULT_CAMERA_POSITION;
		static const Vector3 DEFAULT_CAMERA_TARGET;
		static const float NEAR_PLANE;
		static const float FAR_PLANE;
};

class MathUtils
{
	public:
		static const float PI;
		static const float degreesToRadians;
		static const float radiansToDegrees;
		static const float infinity;
		static const float negativeInfinity;
		static const float epsilon;

		static float degToRad(float degrees);
		static float radToDeg(float radians);
		static float sinDegrees(float angle);
		static float cosDegrees(float angle);
		static float sinRadians(float radians);
		static float cosRadians(float radians);
		//static Line* linearEquation(float a, float b);
		static int primeFactorization(int number);
		static long permutation(int n, int r);
		static int factorial(int number);
		static long combination(int n, int r);
};

class VectorUtils {
	public:
		//static float dotProduct(const GameVector3 & vectorA, const GameVector3 & vectorB);
};

// MathEngine Matrix Operations
class MatrixUtils {
	public:
		//static CustomMatrix getIdentityMatrix(size_t matrixSize);
		//static CustomMatrix getTranslationMatrix(float translationX, float translationY, float translationZ);
		//static CustomMatrix getScaleMatrix(float scaleX, float scaleY, float scaleZ);
		//static CustomMatrix getReflectionMatrix(bool reflectionX, bool reflectionY, bool reflectionZ);
		//static CustomMatrix getRotationMatrix(float degrees, Axis axis);
};