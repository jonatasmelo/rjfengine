#pragma once
#include "GameVector3.h"

class Line
{
public:
	GameVector3 p1;
	GameVector3 p2;

	Line(GameVector3 p1, GameVector3 p2);
	~Line();
};