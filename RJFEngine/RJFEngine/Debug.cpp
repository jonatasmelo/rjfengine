#include "pch.h"
#include "Debug.h"

Debug* Debug::instance = nullptr;

Debug::Debug()
{
}

Debug* Debug::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Debug();
	}

	return instance;
}

void Debug::SetGraphics(Graphics* graphics)
{
	if (GetInstance() != nullptr)
	{
		GetInstance()->graphics = graphics;
	}
}

void Debug::SetColor(XMVECTORF32 color)
{
	if (GetInstance() != nullptr)
	{
		GetInstance()->color = color;
	}
}

XMVECTORF32 Debug::GetColor() 
{
	return GetInstance()->color;
}

void Debug::DrawCollider(Collider* collider)
{
	if (collider == nullptr) 
	{
		return;
	}

	if (GetInstance() != nullptr)
	{
		if (collider->GetColliderType() == ColliderType::SPHERE_COLLIDER) 
		{
			float tesselation = 25;
			float pace = 360.0f / tesselation;

			float x, y, z, px, py, pz;
			float i = 0;

			px = collider->GetCenter().x + collider->GetRadius();
			py = collider->GetCenter().y;
			pz = collider->GetCenter().z;

			while (i <= 360) 
			{
				x = collider->GetCenter().x + MathUtils::cosRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();
				y = collider->GetCenter().y + MathUtils::sinRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();
				z = collider->GetCenter().z;

				GetInstance()->graphics->drawLine(Vector3(px, py, pz), Vector3(x, y, z), GetInstance()->color);

				px = x;
				py = y;
				pz = z;
				i += pace;
			}

			px = collider->GetCenter().x + collider->GetRadius();
			py = collider->GetCenter().y;
			pz = collider->GetCenter().z;
			i = 0;

			while (i <= 360)
			{
				x = collider->GetCenter().x + MathUtils::cosRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();
				y = collider->GetCenter().y;
				z = collider->GetCenter().z + MathUtils::sinRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();

				GetInstance()->graphics->drawLine(Vector3(px, py, pz), Vector3(x, y, z), GetInstance()->color);

				px = x;
				py = y;
				pz = z;
				i += pace;
			}

			px = collider->GetCenter().x;
			py = collider->GetCenter().y + collider->GetRadius();
			pz = collider->GetCenter().z;
			i = 0;

			while (i <= 360)
			{
				x = collider->GetCenter().x;
				y = collider->GetCenter().y + MathUtils::cosRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();
				z = collider->GetCenter().z + MathUtils::sinRadians(i * MathUtils::degreesToRadians) * collider->GetRadius();

				GetInstance()->graphics->drawLine(Vector3(px, py, pz), Vector3(x, y, z), GetInstance()->color);

				px = x;
				py = y;
				pz = z;
				i += pace;
			}
		}

		if (collider->GetColliderType() == ColliderType::BOX_COLLIDER)
		{
			BoxCollider* c = ((BoxCollider*)collider);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z),
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y, collider->GetCenter().z - c->GetBounds()->Extents.z),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y, collider->GetCenter().z + c->GetBounds()->Extents.z),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			//Bottom
			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			//Top
			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			//Sides
			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z - c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				Vector3(collider->GetCenter().x - c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z ),
				GetInstance()->GetColor()
			);

			GetInstance()->graphics->drawLine(
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y - c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z),
				Vector3(collider->GetCenter().x + c->GetBounds()->Extents.x, collider->GetCenter().y + c->GetBounds()->Extents.y , collider->GetCenter().z + c->GetBounds()->Extents.z),
				GetInstance()->GetColor()
			);
		}
	}
}