#include "pch.h"
#include "Physics.h"

Physics* Physics::instance = nullptr;
const float Physics::gravity = 9.8f;

Physics::Physics()
{	
}

Physics* Physics::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Physics();
	}

	return instance;
}

void Physics::ApplyGravityForce(GameObject* obj)
{
	obj->transform->TranslationDown(gravity * 0.1f * Time::elpasedTime);
}