#pragma once
#include <vector>
#include "Game.h"
#include "Scene.h"

using std::vector;

class SceneHandler : public Game
{
	private:
		GameState state;
		std::unique_ptr<Scene> menu;
		std::unique_ptr<Scene> currentScene;
		void changeScene(Scene * newScene);

	protected:
		virtual void initDeviceDependentResources() override;
		virtual void initWindowSizeDependentResources() override;
		virtual void updatePhase(DX::StepTimer const& timer) override;
		virtual void renderPhase() override;
		virtual void resetResources() override;
		virtual vector<GameObject *> * getObjectList() override;

	public:
		SceneHandler();
		SceneHandler(Scene * menu);
		SceneHandler(Scene * startScene, Scene * menu);
		~SceneHandler();
};

