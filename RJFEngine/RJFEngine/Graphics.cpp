#include "pch.h"
#include "Graphics.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

Graphics::Graphics()
{
}

Graphics::Graphics(DX::DeviceResources * deviceResources)
{
	this->deviceResources = deviceResources;
}

Graphics::~Graphics()
{
	resetResources();
}

void Graphics::initDeviceDependentResources(Camera * camera)
{
	this->camera = camera;

	// Matrices
	world = Matrix::Identity;

	auto device = deviceResources->GetD3DDevice();
	
	resourceDescriptors = std::make_unique <DescriptorHeap>(device,
		D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
		D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE,
		Descriptors::Count
	);

	commonStates = std::make_unique<CommonStates>(device);

	// 3D Shape
	shape3d = GeometricPrimitive::CreateTeapot(1.f, 8.f);

	// Model
	testModel = Model::CreateFromSDKMESH(L"tiny.sdkmesh");

	// Primitive Batch
	primitiveBatch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(device);

	// Upload Resources Procedure
	{
		ResourceUploadBatch resourceUpload(device);
		resourceUpload.Begin();

		// Letters Sprite Sheet Texture
		DX::ThrowIfFailed(
			CreateDDSTextureFromFile(device, resourceUpload, L"metal_spritesheet.DDS", lettersSpriteSheetTxt.ReleaseAndGetAddressOf())
		);

		CreateShaderResourceView(device, lettersSpriteSheetTxt.Get(), resourceDescriptors->GetCpuHandle(Descriptors::LettersSpriteSheet));

		// Metal Texture
		DX::ThrowIfFailed(
			CreateDDSTextureFromFile(device, resourceUpload, L"MetalTexture.DDS", metalTxt.ReleaseAndGetAddressOf())
		);

		CreateShaderResourceView(device, metalTxt.Get(), resourceDescriptors->GetCpuHandle(Descriptors::MetalTexture));

		RenderTargetState rtState(deviceResources->GetBackBufferFormat(), deviceResources->GetDepthBufferFormat());

		// Sprite Batch
		{
			SpriteBatchPipelineStateDescription spritePipeline(rtState);

			spriteBatch = std::make_unique<SpriteBatch>(device, resourceUpload, spritePipeline);
		}

		// Shape 3D Effect
		{
			EffectPipelineStateDescription shape3dPipeline(
				&GeometricPrimitive::VertexType::InputLayout,
				CommonStates::Opaque,
				CommonStates::DepthDefault,
				CommonStates::CullNone,
				rtState
			);

			basicEffect3d = std::make_unique<BasicEffect>(device, EffectFlags::Lighting | EffectFlags::Texture, shape3dPipeline);
			basicEffect3d->EnableDefaultLighting();
			basicEffect3d->SetTexture(resourceDescriptors->GetGpuHandle(Descriptors::MetalTexture), commonStates->LinearWrap());
		}

		// Model
		modelResources = testModel->LoadTextures(device, resourceUpload);

		{
			EffectPipelineStateDescription modelPipeline(
				nullptr,
				CommonStates::Opaque,
				CommonStates::DepthDefault,
				CommonStates::CullNone,
				rtState
			);

			modelEffect = testModel->CreateEffects(modelPipeline, modelPipeline, modelResources->Heap(), commonStates->Heap());
		}

		// Upload the resources to the GPU
		auto uploadResourcesFinished = resourceUpload.End(deviceResources->GetCommandQueue());

		// Wait for the command list to finish executing
		deviceResources->WaitForGpu();

		// Wait for the upload thread to terminate
		uploadResourcesFinished.wait();
	}

}

void Graphics::initWindowSizeDependentResources()
{
	// Viewport
	auto viewport = deviceResources->GetScreenViewport();
	spriteBatch->SetViewport(viewport);

	updateProjectionMatrix();
	updateViewMatrix();
}

void Graphics::resetResources()
{
	// Shapes
	shape3d.reset();

	// Textures
	lettersSpriteSheetTxt.Reset();
	metalTxt.Reset();

	// Model
	testModel.reset();

	// Batches
	spriteBatch.reset();
	primitiveBatch.reset();

	// Effects
	basicEffect3d.reset();
	commonStates.reset();

	// Resources
	resourceDescriptors.reset();
}

void Graphics::updateViewMatrix()
{
	basicEffect3d->SetView(camera->getView());
}

void Graphics::updateProjectionMatrix()
{
	basicEffect3d->SetProjection(camera->projection);
}

void Graphics::drawLine(Vector3 start, Vector3 end, XMVECTORF32 color)
{
	auto commandList = deviceResources->GetCommandList();

	ResourceManager::getFXLine()->Apply(commandList);
	primitiveBatch->Begin(commandList);

	VertexPositionColor v1(start, color);
	VertexPositionColor v2(end, color);
	primitiveBatch->DrawLine(v1, v2);

	primitiveBatch->End();
}

void Graphics::drawSprite(float posX, float posY)
{
	auto commandList = deviceResources->GetCommandList();
	ID3D12DescriptorHeap * heaps[] = { resourceDescriptors->Heap(), commonStates->Heap() };
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);

	PIXBeginEvent(commandList, PIX_COLOR_DEFAULT, L"Draw Letters Sprite Sheet");

	RECT rectTest;
	rectTest.left = 0;
	rectTest.top = 0;
	rectTest.right = 256;
	rectTest.bottom = 256;

	spriteBatch->Begin(commandList);
	//spriteBatch->Draw(resourceDescriptors->GetGpuHandle(Descriptors::LettersSpriteSheet), GetTextureSize(lettersSpriteSheetTxt.Get()), XMFLOAT2(0, 0), nullptr, Colors::White, 0.f, XMFLOAT2(0, 0), 0.5f);
	spriteBatch->Draw(resourceDescriptors->GetGpuHandle(Descriptors::LettersSpriteSheet), GetTextureSize(lettersSpriteSheetTxt.Get()), XMFLOAT2(0, 0), &rectTest, Colors::White, 0.f, XMFLOAT2(0,0), 0.5f);

	spriteBatch->End();

	PIXEndEvent(commandList);

	PIXBeginEvent(commandList, PIX_COLOR_DEFAULT, L"Draw Teapot");

	basicEffect3d->SetWorld(world * Matrix::CreateRotationY(XM_PI / 4.f) * Matrix::CreateTranslation({posX, posY, 0.f}));
	basicEffect3d->Apply(commandList);
	shape3d->Draw(commandList);

	PIXEndEvent(commandList);

	PIXBeginEvent(commandList, PIX_COLOR_DEFAULT, L"Draw Model");

	const XMVECTORF32 scale = { 0.01f, 0.01f, 0.01f };
	const XMVECTORF32 translate = { 3.f, -2.f, -4.f };
	XMVECTOR rotate = Quaternion::CreateFromYawPitchRoll(0.f, -XM_PI / 2.f, 0.f);
	XMMATRIX matrixLocal = world *XMMatrixTransformation(g_XMZero, Quaternion::Identity, scale, g_XMZero, rotate, translate);
	Model::UpdateEffectMatrices(modelEffect, matrixLocal, camera->getView(), camera->projection);
	heaps[0] = modelResources->Heap();
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	testModel->Draw(commandList, modelEffect.begin());
	//testModel->DrawOpaque(commandList, modelEffect.begin());
	//testModel->DrawAlpha(commandList, modelEffect.begin());

	PIXEndEvent(commandList);
}
