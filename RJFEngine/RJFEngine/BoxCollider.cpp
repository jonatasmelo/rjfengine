#include "pch.h"
#include "BoxCollider.h"

BoxCollider::BoxCollider() : Collider(ColliderType::BOX_COLLIDER)
{
	this->boundingBox = new BoundingBox();
	this->enabled = true;
}

BoxCollider::~BoxCollider()
{
}

BoundingBox* BoxCollider::GetBounds() {
	return boundingBox;
}