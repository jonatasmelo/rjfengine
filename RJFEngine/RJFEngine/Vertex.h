#pragma once

#include "PrimitiveShape.h"

class Vertex : public PrimitiveShape
{
public:
	Vector3 point;
	BoundingBox boxCollider;
	float size;

	Vertex();
	Vertex(const Vertex & vertex);
	Vertex(Vector3 point, float size);
	~Vertex();

	void init() override;
	void update() override;
	void draw(Graphics * graphics) override;
	void destroy() override;

	void setPoint(Vector3 point);

	void calculateCenter(float &centerX, float &centerY, bool resetObjBeforeTransformation) override;
};