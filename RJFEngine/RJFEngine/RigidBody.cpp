#include "pch.h"
#include "RigidBody.h"

RigidBody::RigidBody(bool reactsWithGravity, bool isKinematic) : ObjectComponent(ComponentType::RIGID_BODY)
{
	this->reactsWithGravity = reactsWithGravity;
	this->isKinematic = isKinematic;
}

RigidBody::~RigidBody()
{
}