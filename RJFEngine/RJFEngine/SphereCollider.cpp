#include "pch.h"
#include "SphereCollider.h"
#include "BoxCollider.h"

SphereCollider::SphereCollider() :  Collider(ColliderType::SPHERE_COLLIDER)
{
	this->boundingSphere = new BoundingSphere();
	this->enabled = true;
}

SphereCollider::~SphereCollider()
{
}

BoundingSphere* SphereCollider::GetBounds()
{
	return boundingSphere;
}