#include "pch.h"
#include "LineShape.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

LineShape::LineShape()
{
	init();
}

LineShape::LineShape(const LineShape & line)
{
	vectors = line.vectors;
	transformations = line.transformations;
	color = line.color;
}

LineShape::~LineShape()
{
}

void LineShape::init() {
	vectors = CustomMatrix(2, Dimensions::FOUR_DIMENSIONS);

	//transformations = MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS);

	color = Colors::White;
}

void LineShape::update() {
}

void LineShape::draw(Graphics * graphics) {
	// Drawing Rectangle
	Vector3 start, end;

	CustomMatrix transformedVectors = vectors * transformations;

	// Top Left
	start.x = transformedVectors[CustomLine::START_POINT][Axis::X];
	start.y = transformedVectors[CustomLine::START_POINT][Axis::Y];
	start.z = transformedVectors[CustomLine::START_POINT][Axis::Z];

	// Top Right
	end.x = transformedVectors[CustomLine::END_POINT][Axis::X];
	end.y = transformedVectors[CustomLine::END_POINT][Axis::Y];
	end.z = transformedVectors[CustomLine::END_POINT][Axis::Z];

	//graphics->drawLine(start, end, color);
}

void LineShape::destroy() {
}

void LineShape::calculateCenter(float & centerX, float & centerY, bool resetObjBeforeTransformation) {
	CustomMatrix transformedVectors = vectors;

	if (!resetObjBeforeTransformation) {
		transformedVectors = transformedVectors * transformations;
	}

	centerX = transformedVectors[CustomLine::START_POINT][Axis::X];
	centerY = transformedVectors[CustomLine::START_POINT][Axis::Y];
}
