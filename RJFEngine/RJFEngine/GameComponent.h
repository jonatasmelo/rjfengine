#pragma once
#include "Graphics.h"

class GameComponent abstract 
{
	public:
		virtual void init() = 0;
		virtual void update() = 0;
		virtual void draw(Graphics * graphics) = 0;
		virtual void destroy() = 0;
};