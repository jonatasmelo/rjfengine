#include "pch.h"
#include "Terrain.h"
#include "bitmap_image.hpp"

Terrain::Terrain()
{
	width = Terrain::MAX_DETAIL;
	height = Terrain::MAX_DETAIL;
}

Terrain::Terrain(int width, int height)
{
	this->width = width > Terrain::MAX_DETAIL ? Terrain::MAX_DETAIL : width;
	this->height = height > Terrain::MAX_DETAIL ? Terrain::MAX_DETAIL : height;
}

Terrain::Terrain(const Terrain &)
{
}

Terrain::~Terrain()
{
}

bool Terrain::loadHeightMap(std::string fileName)
{
	bitmap_image image(fileName);
	if (!image) {
		return false;
	}

	int index;

	width = image.width() > Terrain::MAX_DETAIL ? Terrain::MAX_DETAIL : image.width();
	height = image.height() > Terrain::MAX_DETAIL ? Terrain::MAX_DETAIL : image.height();

	heightMap = new Vector3[width * height];

	for (std::size_t z = 0; z < height; z++)
	{
		for (std::size_t x = 0; x < width; x++)
		{
			rgb_t colour;
			image.get_pixel(x, z, colour);

			index = (height * z) + x;
			heightMap[index].x = (float)x;
			heightMap[index].y = (float)colour.red;
			heightMap[index].z = (float)z;
		}
	}

	return true;
}

void Terrain::normalizeHeightMap()
{
	for (int z = 0; z < height; z++)
	{
		for (int x = 0; x < width; x++)
		{
			heightMap[(height * z) + x].y /= 15.f;
		}
	}
}

void Terrain::init()
{
	loadHeightMap("heightmap02.bmp");
	normalizeHeightMap();

	int index = 0;
	int index1, index2, index3, index4;

	vertexCount = (width - 1) * (height - 1) * 12;
	indexCount = vertexCount;

	vertexList = new Vector3[vertexCount];
	indices = new unsigned short[indexCount];

	for (int j = 0; j < (height - 1); j++) 
	{
		for (int i = 0; i < (width - 1); i++) 
		{
			// Bottom Left
			index1 = (height * j) + i;
			// Bottom Right
			index2 = (height * j) + (i + 1);
			// Upper Left
			index3 = (height * (j + 1)) + i;
			// Upper Right
			index4 = (height * (j + 1)) + (i + 1);

			// Upper Left
			vertexList[index] = Vector3(heightMap[index3].x, heightMap[index3].y, heightMap[index3].z);
			indices[index] = index;
			index++;

			// Upper Right
			vertexList[index] = Vector3(heightMap[index4].x, heightMap[index4].y, heightMap[index4].z);
			indices[index] = index;
			index++;

			// Upper Right
			vertexList[index] = Vector3(heightMap[index4].x, heightMap[index4].y, heightMap[index4].z);
			indices[index] = index;
			index++;

			// Bottom Left
			vertexList[index] = Vector3(heightMap[index1].x, heightMap[index1].y, heightMap[index1].z);
			indices[index] = index;
			index++;

			// Bottom Left
			vertexList[index] = Vector3(heightMap[index1].x, heightMap[index1].y, heightMap[index1].z);
			indices[index] = index;
			index++;

			// Upper Left
			vertexList[index] = Vector3(heightMap[index3].x, heightMap[index3].y, heightMap[index3].z);
			indices[index] = index;
			index++;

			// Bottom Left
			vertexList[index] = Vector3(heightMap[index1].x, heightMap[index1].y, heightMap[index1].z);
			indices[index] = index;
			index++;

			// Upper Right
			vertexList[index] = Vector3(heightMap[index4].x, heightMap[index4].y, heightMap[index4].z);
			indices[index] = index;
			index++;

			// Upper Right
			vertexList[index] = Vector3(heightMap[index4].x, heightMap[index4].y, heightMap[index4].z);
			indices[index] = index;
			index++;

			// Bottom Right
			vertexList[index] = Vector3(heightMap[index2].x, heightMap[index2].y, heightMap[index2].z);
			indices[index] = index;
			index++;

			// Bottom Right
			vertexList[index] = Vector3(heightMap[index2].x, heightMap[index2].y, heightMap[index2].z);
			indices[index] = index;
			index++;

			// Bottom Left
			vertexList[index] = Vector3(heightMap[index1].x, heightMap[index1].y, heightMap[index1].z);
			indices[index] = index;
			index++;
		}
	}
}

void Terrain::draw(Graphics * graphics)
{
	//graphics->drawVertices(vertexList, vertexCount, Colors::White, indices, indexCount);
}