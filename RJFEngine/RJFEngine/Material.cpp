#include "pch.h"
#include "Material.h"

Material::Material(size_t index, std::wstring textureName, bool lighting, WrapMode wrapMode) : ObjectComponent(ComponentType::MATERIAL)
{
	this->index = index;
	this->texture = textureName;
	this->lighting = lighting;
	this->wrapMode = wrapMode;
}

Material::~Material()
{

}

void Material::applyEffects(ID3D12GraphicsCommandList * commandList)
{
	effect->Apply(commandList);
}

void Material::setEffect(BasicEffect * effect)
{
	this->effect = std::unique_ptr<BasicEffect>(effect);
}

BasicEffect * Material::getEffect()
{
	return effect.get();
}

ID3D12Resource ** Material::clearTextureResource() {
	return textureResource.ReleaseAndGetAddressOf();
}

ID3D12Resource * Material::getTextureResource()
{
	return textureResource.Get();
}
