#include "pch.h"
#include "GameVector4.h"

float& GameVector4::operator[](const unsigned int index)
{
	switch (index)
	{
		case Axis::X:
			return x;

		case Axis::Y:
			return y;

		case Axis::Z:
			return z;

		case Axis::W:
			return w;

		default:
			throw new LogicErrorException("Invalid Axis");
	}

}

GameVector4 & GameVector4::operator+(const GameVector4 & vector4)
{
	GameVector4 * result = new GameVector4(vector4);

	result->x += this->x;
	result->y += this->y;
	result->z += this->z;
	result->w = 1;

	return *result;
}

GameVector4 & GameVector4::operator*(const GameVector4 & vector)
{
	// x = ay*bz - az*by
	// y = -1 * (ax*bz - az*bx)
	// z = ax*by - ay*bx
	GameVector4 * result = new GameVector4();

	result->x = this->y * vector.z - this->z * vector.y;
	result->y = -1 * (this->x * vector.z - this->z * vector.x);
	result->z = this->x * vector.y - this->y * vector.x;
	result->w = 1;

	return *result;
}

GameVector4::GameVector4()
{
	GameVector3();
	w = 1;
}

GameVector4::GameVector4(const GameVector4 & vector4)
{
	x = vector4.x;
	y = vector4.y;
	z = vector4.z;
	w = vector4.w;
}

GameVector4::GameVector4(float x, float y) : GameVector3(x, y)
{
	this->w = 1;
}

GameVector4::GameVector4(float x, float y, float z) : GameVector3(x, y, z)
{
	this->w = 1;
}

GameVector4::GameVector4(float valueX, float valueY, float valueZ, float valueW) : GameVector3(valueX, valueY, valueZ)
{
	w = valueW;
}


GameVector4::~GameVector4()
{
}

GameVector4 & operator*(float scalar, const GameVector4 & vector4)
{
	GameVector4 * result = new GameVector4(vector4);

	result->x *= scalar;
	result->y *= scalar;
	result->z *= scalar;

	return *result;
}

GameVector4 & operator*(const GameVector4 & vector4, float scalar)
{
	return scalar * vector4;
}

GameVector4 & operator/(float scalar, const GameVector4 & vector4)
{
	GameVector4 * result = new GameVector4(vector4);

	result->x /= scalar;
	result->y /= scalar;
	result->z /= scalar;

	return *result;
}

GameVector4 & operator/(const GameVector4 & vector4, float scalar)
{
	return scalar / vector4;
}
