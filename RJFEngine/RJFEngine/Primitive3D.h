#pragma once

#include "ObjectComponent.h"

enum Primitive3DType {
	CUBE = 0,
	SPHERE = 1,
	TEAPOT = 2,
	BOX = 3
};

class Primitive3D : public ObjectComponent
{
private:
	std::unique_ptr<DirectX::GeometricPrimitive> shape;

public:
	Primitive3D(Primitive3DType type, float size, size_t tesselation);
	Primitive3D(Primitive3DType type, Vector3 area);
	~Primitive3D();

	float size;
	Vector3 area;

	const Primitive3DType primitiveType;

	GeometricPrimitive* GetShape();
};