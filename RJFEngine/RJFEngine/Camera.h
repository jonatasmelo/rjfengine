#pragma once

#include <string>
#include "Transform.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

using std::unique_ptr;
using std::string;

class Camera
{

public:
	static const Vector3 UP_DIRECTION;

	int id;
	string name;

	float fov;
	float nearPlane;
	float farPlane;
	float aspectRatio;
	float screenWidth;
	float screenHeight;

	Transform * transform = nullptr;
	Vector3 target;

	Matrix projection;

	Camera(float fov, float screeWidth, float screenHeight, float nearPlane, float farPlane, Vector3 position, Vector3 target);
	~Camera();

	Matrix getProjection();
	Matrix getView();

	Ray screenToWorldRay(float screenX, float screenY, Matrix world);
	Vector3 screenToWorldPoint(float screenX, float screenY);

	void setScreenSize(float width, float height);
	void setTarget(Vector3 position);

	void Translate(Vector3 axis);
	void Translation(Vector3 axis, float speed);
	void TranslationUp(float speed);
	void TranslationDown(float speed);
	void TranslationLeft(float speed);
	void TranslationRight(float speed);
	void TranslationBack(float speed);
	void TranslationForth(float speed);

	void Rotate(Quaternion rotation);
	void Rotate(Vector3 axis, float angle);
	void Rotation(Vector3 axis, float speed);

	void LookAt(Vector3 target);
	void LookAt(GameObject* target);
};