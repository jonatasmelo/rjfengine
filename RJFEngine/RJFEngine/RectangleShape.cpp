#include "pch.h"
#include "RectangleShape.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

RectangleShape::RectangleShape()
{
	init();
}

RectangleShape::RectangleShape(const RectangleShape & rectangle)
{
	vectors = rectangle.vectors;
	transformations = rectangle.transformations;
	color = rectangle.color;
}

RectangleShape::~RectangleShape()
{
}

void RectangleShape::init() {
	vectors = CustomMatrix(4, Dimensions::FOUR_DIMENSIONS);
	//transformations = MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS);
	color = Colors::Red;
}

void RectangleShape::update() {
}

void RectangleShape::draw(Graphics * graphics) {
	// Drawing Rectangle
	Vector3 upperLeft, upperRight, lowerRight, lowerLeft;
	CustomMatrix transformedVectors = vectors * transformations;

	// Top Left
	upperLeft.x = transformedVectors[CustomRectangle::UPPER_LEFT][Axis::X];
	upperLeft.y = transformedVectors[CustomRectangle::UPPER_LEFT][Axis::Y];
	upperLeft.z = transformedVectors[CustomRectangle::UPPER_LEFT][Axis::Z];

	// Top Right
	upperRight.x = transformedVectors[CustomRectangle::UPPER_RIGHT][Axis::X];
	upperRight.y = transformedVectors[CustomRectangle::UPPER_RIGHT][Axis::Y];
	upperRight.z = transformedVectors[CustomRectangle::UPPER_RIGHT][Axis::Z];

	// Bottom Right
	lowerRight.x = transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::X];
	lowerRight.y = transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::Y];
	lowerRight.z = transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::Z];

	// Bottom Left
	lowerLeft.x = transformedVectors[CustomRectangle::LOWER_LEFT][Axis::X];
	lowerLeft.y = transformedVectors[CustomRectangle::LOWER_LEFT][Axis::Y];
	lowerLeft.z = transformedVectors[CustomRectangle::LOWER_LEFT][Axis::Z];

	//graphics->drawRectangle(upperLeft, upperRight, lowerRight, lowerLeft, color);
}

void RectangleShape::destroy() {
}

void RectangleShape::calculateCenter(float & centerX, float & centerY, bool resetObjBeforeTransformation) {
	CustomMatrix transformedVectors = vectors;

	if (!resetObjBeforeTransformation) {
		transformedVectors = transformedVectors * transformations;
	}

	centerX = (transformedVectors[CustomRectangle::UPPER_LEFT][Axis::X] + transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::X]) / 2;
	centerY = (transformedVectors[CustomRectangle::UPPER_LEFT][Axis::Y] + transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::Y]) / 2;
}
