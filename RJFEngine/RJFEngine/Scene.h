#pragma once
#include <vector>
#include "StepTimer.h"
#include "Graphics.h"
#include "Camera.h"

using std::vector;

class Scene
{
	protected:
		Camera * camera;
		std::unique_ptr<vector<GameObject *>> objects;

	public:
		Scene();

		virtual void initDeviceDependentResources();
		virtual void initWindowSizeDependentResources();
		virtual bool update(DX::StepTimer const& timer);
		virtual Scene * render(Graphics * graphics);
		virtual void reset();

		void setCamera(Camera * camera);
		vector<GameObject *> * getObjectList();
};