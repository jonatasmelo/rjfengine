#pragma once

#include "Processor.h"

class TransformProcessor : Processor
{
public:
	TransformProcessor();
	~TransformProcessor();

	void Execute(GameObject* objA) override;
};