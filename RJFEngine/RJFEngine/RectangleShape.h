#pragma once
#include "pch.h"
#include "PrimitiveShape.h"

class RectangleShape : public PrimitiveShape
{
	public:
		RectangleShape();
		RectangleShape(const RectangleShape & rectangle);
		~RectangleShape();

		void init() override;
		void update() override;
		void draw(Graphics * graphics) override;
		void destroy() override;

		void calculateCenter(float &centerX, float &centerY, bool resetObjBeforeTransformation) override;
};

