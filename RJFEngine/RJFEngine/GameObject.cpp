#include "pch.h"
#include "GameObject.h"

GameObject::GameObject(string name)
{
	this->enabled = true;
	this->name = name;
	this->transform = new Transform();
}

GameObject::~GameObject()
{
}

int GameObject::GetComponentIndex(ComponentType type)
{
	if (components.empty())
	{
		return -1;
	}
	for (int i = 0; i < components.size(); i++)
	{
		if (components.at(i)->GetComponentType() == type)
		{
			return i;
		}
	}

	return -1;
}

bool GameObject::HasComponent(ComponentType type)
{
	return GetComponentIndex(type) > -1;
}

void GameObject::AddComponent(ObjectComponent* component)
{
	if (component != nullptr)
	{
		component->gameObject = this;

		if (component->GetComponentType() == ComponentType::TRANSFORM) {
			if (transform == nullptr) {
				components.push_back(component);
			}

			transform = (Transform*)component;
		}
		else {
			switch (component->GetComponentType()) {
			case ComponentType::PRIMITIVE_3D:
				if (primitive3D == nullptr) {
					components.push_back(component);
				}
				primitive3D = (Primitive3D*)component;
				break;
			case ComponentType::MATERIAL:
				if (material == nullptr) {
					components.push_back(component);
				}
				material = (Material*)component;
				break;
			case ComponentType::COLLIDER:
				if (collider == nullptr) {
					components.push_back(component);
				}
				collider = (Collider*)component;
				collider->SetCenter(transform->getPosition());

				if (primitive3D != nullptr) {
					if(primitive3D->primitiveType != Primitive3DType::BOX)
					{
						if (collider->GetColliderType() == ColliderType::BOX_COLLIDER) {
							collider->SetSize(Vector3(primitive3D->size / 2.0f));
						}
						else if (this->collider->GetColliderType() == ColliderType::SPHERE_COLLIDER) {
							collider->SetRadius(primitive3D->size / 2.0f);
						}
					}
					else {
						collider->SetSize(Vector3(primitive3D->area.x / 2.0f, primitive3D->area.y / 2.0f, primitive3D->area.z / 2.0f));
					}
				}
				else {
					if (collider->GetColliderType() == ColliderType::BOX_COLLIDER) {
						collider->SetSize(Vector3(0.5f));
					}
					else if (this->collider->GetColliderType() == ColliderType::SPHERE_COLLIDER) {
						collider->SetRadius(0.5f);
					}
				}

				break;
			case ComponentType::RIGID_BODY:
				if (this->rigidBody == nullptr) {
					components.push_back(component);
				}
				this->rigidBody = (RigidBody*)component;
				break;
			case ComponentType::BEHAVIOUR:
				if (this->behaviour == nullptr) {
					components.push_back(component);
				}
				this->behaviour = (Behaviour*)component;
				break;
			case ComponentType::CAMERA:
				components.push_back(component);
				break;
			}
		}		
	}
}

void GameObject::RemoveComponent(ComponentType type)
{
	vector<ObjectComponent*>::iterator it = components.begin();
	advance(it, GetComponentIndex(type));
	components.erase(it);

	switch (type) {
	case ComponentType::PRIMITIVE_3D:
		this->primitive3D = nullptr;
		break;
	case ComponentType::MATERIAL:
		this->material = nullptr;
		break;
	case ComponentType::COLLIDER:
		this->collider = nullptr;
		break;
	case ComponentType::RIGID_BODY:
		this->rigidBody = nullptr;
		break;
	case ComponentType::BEHAVIOUR:
		this->behaviour = nullptr;
		break;
	}
}

ObjectComponent* GameObject::GetComponentByType(ComponentType type) 
{
	ObjectComponent* component = nullptr;

	for (int i = 0; i < components.size(); i++)
	{
		component = components.at(i);

		if (component->GetComponentType() == type)
		{
			return component;
		}
	}
	
	return nullptr;
}

Vector3 GameObject::GetPosition()
{
	return transform->getPosition();
}

bool GameObject::HasValidTransform()
{
	return this->transform != nullptr && this->transform->enabled;
}

bool GameObject::HasValidPrimitive3D()
{
	return this->primitive3D != nullptr && this->primitive3D->enabled;
}

bool GameObject::HasValidCollider()
{
	return this->collider != nullptr && this->collider->enabled;
}

bool GameObject::HasValidRigidBody()
{
	return this->rigidBody != nullptr && this->rigidBody->enabled;
}

bool GameObject::HasValidBehaviour()
{
	return this->behaviour != nullptr && this->behaviour->enabled;
}

bool GameObject::HasValidMaterial()
{
	return this->material != nullptr && this->material->enabled;
}