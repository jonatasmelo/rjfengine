#pragma once

#include <unordered_set>
#include "CollisionProcessor.h"

using namespace std;

enum ColliderType {
	BOX_COLLIDER = 0,
	SPHERE_COLLIDER = 1
};

class Collider abstract : public ObjectComponent
{
	friend class CollisionProcessor;

private:
	unordered_set <int>::iterator hashIterator;
	unordered_set<int> collidingIds;
	bool AddIdToCollidingList(int id);
	bool ContainsInCollidingList(int id);
	bool RemoveFromCollidingList(int id);

protected:
	ColliderType colliderType;

	Vector3 center;
	Vector3 size;
	float radius;

	BoundingBox* boundingBox = nullptr;
	BoundingSphere* boundingSphere = nullptr;

public:	
	Collider(ColliderType colliderType);
	~Collider();

	ColliderType GetColliderType();

	bool Intersects(Ray ray, float distance);
	bool IsColliding(Collider* other);

	void Transform();

	virtual Vector3 GetSize();
	virtual void SetSize(Vector3 size);
	virtual float GetRadius();
	virtual void SetRadius(float radius);
	Vector3 GetCenter();
	void SetCenter(Vector3 center);
};