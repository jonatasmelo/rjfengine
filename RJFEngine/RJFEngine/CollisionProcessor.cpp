#include "pch.h"
#include "CollisionProcessor.h"
#include <math.h>

CollisionProcessor::CollisionProcessor()
{
}

CollisionProcessor::~CollisionProcessor()
{
}

void CollisionProcessor::Execute(GameObject* objA)
{
	// Remember to call updateObjectList before this
	if (objects != nullptr && !objA->staticObject && objA->HasValidCollider())
	{
		objA->collider->Transform();

		for (GameObject* objB : *objects)
		{
			if (objB->enabled)
			{
				if (objA->id != objB->id && objB->HasValidCollider())
				{
					if (objA->collider->IsColliding(objB->collider))
					{
						if (objA->collider->AddIdToCollidingList(objB->id))
						{
							if (objA->HasValidBehaviour())
							{
								objA->behaviour->OnCollisionEnter(objB);
							}
							if (objB->HasValidBehaviour())
							{
								objB->behaviour->OnCollisionEnter(objA);
							}
						}
						else
						{
							if (objA->HasValidBehaviour())
							{
								objA->behaviour->OnCollisionStay(objB);
							}
							if (objB->HasValidBehaviour())
							{
								objB->behaviour->OnCollisionStay(objA);
							}
						}

						if (!(objA->staticObject && objB->staticObject))
						{
							if (objB->staticObject && !objA->staticObject)
							{
								if (objB->GetPosition().y >= objA->GetPosition().y)
								{							
									Vector3 direction = (objB->GetPosition() - objA->GetPosition());
									
									direction.y = 0;
									
									objA->transform->Translation(direction * -1, 0.1f);
								}
								else
								{
									objA->transform->TranslationUp(Physics::gravity * 0.1f * Time::elpasedTime);
									objA->collider->Transform();
								}
							}
							else
							{
								if (objA->HasValidRigidBody() && objA->rigidBody->reactsWithGravity)
								{
									Vector3 direction = objB->GetPosition() - objA->GetPosition();
									
									direction.y = 0;

									if (std::abs(direction.x) > std::abs(direction.z)) {
										direction.z = 0;
									}
									else {
										direction.x = 0;
									}

									objB->transform->Translation(direction, 0.05f);
								}
							}
						}
					}
					else
					{
						if (objA->collider->RemoveFromCollidingList(objB->id))
						{
							if (objA->HasValidBehaviour())
							{
								objA->behaviour->OnCollisionExit(objB);
							}
							if (objB->HasValidBehaviour())
							{
								objB->behaviour->OnCollisionExit(objA);
							}
						}
					}
				}
			}
		}
	}
}

void CollisionProcessor::updateObjectList(vector<GameObject *> * objects) {
	this->objects = objects;
}