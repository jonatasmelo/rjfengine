#pragma once

class BoxCollider : public Collider
{
public:
	BoxCollider();
	~BoxCollider();

	BoundingBox* GetBounds();
};