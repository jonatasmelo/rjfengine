#pragma once
#include "pch.h"
#include "PrimitiveShape.h"

class LineShape : public PrimitiveShape
{
	public:
		LineShape();
		LineShape(const LineShape & line);

		~LineShape();

		void init() override;
		void update() override;
		void draw(Graphics * graphics) override;
		void destroy() override;

		void calculateCenter(float &centerX, float &centerY, bool resetObjBeforeTransformation) override;
};

