#pragma once
#include "pch.h"
#include "DeviceResources.h"
#include "Camera.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Graphics
{
	protected:
		DX::DeviceResources * deviceResources;
		std::unique_ptr<DescriptorHeap> resourceDescriptors;
		
		//Camera
		Camera * camera;

		// Batch
		std::unique_ptr<SpriteBatch> spriteBatch;
		std::unique_ptr<PrimitiveBatch<VertexPositionColor>> primitiveBatch;

		// Shapes
		std::unique_ptr<GeometricPrimitive> shape3d;

		// Effects
		std::unique_ptr<BasicEffect> basicEffect3d;
		std::unique_ptr<CommonStates> commonStates;

		// Textures
		Microsoft::WRL::ComPtr<ID3D12Resource> lettersSpriteSheetTxt;
		Microsoft::WRL::ComPtr<ID3D12Resource> metalTxt;

		// Model
		std::unique_ptr<Model> testModel;
		std::unique_ptr<EffectTextureFactory> modelResources;
		std::vector<std::shared_ptr<IEffect>> modelEffect;

		enum Descriptors
		{
			LettersSpriteSheet,
			MetalTexture,
			Count
		};

	public:
		// Matrices
		Matrix world;

		Graphics();
		Graphics(DX::DeviceResources * deviceResources);
		~Graphics();

		void initDeviceDependentResources(Camera * camera);
		void initWindowSizeDependentResources();
		void resetResources();

		void updateViewMatrix();
		void updateProjectionMatrix();

		BasicEffect * getBasicEffectPrimitive();

		// Basic Shapes
		// 2D
		void drawLine(Vector3 start, Vector3 end, XMVECTORF32 color);

		// Test
		void drawSprite(float posX, float posY);
};

