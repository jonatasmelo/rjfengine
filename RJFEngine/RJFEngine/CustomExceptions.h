#pragma once
#include <exception>

using std::exception;

class ShutdownGameException : public exception {
	public:
		ShutdownGameException() : exception("") {};
		ShutdownGameException(char * msg) : exception(msg) {};
};

class LogicErrorException : public exception {
	public:
		LogicErrorException() : exception("") {};
		LogicErrorException(char * msg) : exception(msg) {};
};