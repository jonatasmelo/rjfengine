#pragma once
#include <vector>

using std::vector;

class TerrainHeigtMap
{
	protected:
		size_t levelOfDetail;
		size_t width;
		vector<float> data;

		size_t index(size_t x, size_t y) const;
		void setLevelOfDetail(size_t value);

	public:
		TerrainHeigtMap(size_t getLevelOfDetail);
		~TerrainHeigtMap();

		float at(size_t x, size_t y) const;
		float & at(size_t x, size_t y);
		size_t getLevelOfDetail() const;
		size_t getWidth() const;
		size_t getHeight() const;
		size_t size() const;
		const float * getData() const;
};

