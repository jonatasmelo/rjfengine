#pragma once

#include "Processor.h"

using std::vector;

class CollisionProcessor : Processor
{
private:
	vector<GameObject*>* objects = nullptr;

public:
	CollisionProcessor();
	~CollisionProcessor();

	void Execute(GameObject* objA) override;
	void updateObjectList(vector<GameObject *> * objects);
};
