#include "pch.h"
#include "GameVector3.h"

float& GameVector3::operator[](const unsigned int index)
{
	switch (index)
	{
		case Axis::X:
			return x;

		case Axis::Y:
			return y;

		case Axis::Z:
			return z;

		default:
			throw new LogicErrorException("Invalid Axis");
		break;
	}
	
}

GameVector3 & GameVector3::operator+(const GameVector3 & vector3)
{
	GameVector3 * result = new GameVector3(vector3);

	result->x += this->x;
	result->y += this->y;
	result->z += this->z;

	return *result;
}

GameVector3 & GameVector3::operator*(const GameVector3 & vector)
{
	// x = ay*bz - az*by
	// y = -1 * (ax*bz - az*bx)
	// z = ax*by - ay*bx
	GameVector3 * result = new GameVector3();

	result->x = this->y * vector.z - this->z * vector.y;
	result->y = -1 * (this->x * vector.z - this->z * vector.x);
	result->z = this->x * vector.y - this->y * vector.x;

	return *result;
}

GameVector3::GameVector3()
{
	x = 0;
	y = 0;
	z = 0;
}

GameVector3::GameVector3(const GameVector3 & vector3)
{
	x = vector3.x;
	y = vector3.y;
	z = vector3.z;
}

GameVector3::GameVector3(float x, float y)
: x(x), y(y), z(0) {}

GameVector3::GameVector3(float valueX, float valueY, float valueZ)
{
	x = valueX;
	y = valueY;
	z = valueZ;
}


GameVector3::~GameVector3()
{
}

void GameVector3::invert()
{
	this->x *= -1;
	this->y *= -1;
	this->z *= -1;
}

float GameVector3::getMagnitude()
{
	return std::sqrt(std::pow(x,2) + std::pow(y, 2) + std::pow(z, 2));
}

float GameVector3::dotProduct(const GameVector3 & vector)
{
	//return VectorUtils::dotProduct(*this, vector);
	return 0.f;
}

GameVector3 & operator*(float scalar, const GameVector3 & vector3)
{
	GameVector3 * result = new GameVector3(vector3);

	result->x *= scalar;
	result->y *= scalar;
	result->z *= scalar;

	return *result;
}

GameVector3 & operator*(const GameVector3 & vector3, float scalar)
{
	return scalar * vector3;
}

GameVector3 & operator/(float scalar, const GameVector3 & vector3)
{
	GameVector3 * result = new GameVector3(vector3);

	result->x /= scalar;
	result->y /= scalar;
	result->z /= scalar;

	return *result;
}

GameVector3 & operator/(const GameVector3 & vector3, float scalar)
{
	return scalar / vector3;
}
