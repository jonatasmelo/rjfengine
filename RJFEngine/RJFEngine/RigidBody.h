#pragma once

#include "ObjectComponent.h"

class RigidBody : public ObjectComponent
{
public:
	bool reactsWithGravity;
	bool isKinematic;

	RigidBody(bool gravityEnabled, bool isKinematic = false);
	~RigidBody();
};