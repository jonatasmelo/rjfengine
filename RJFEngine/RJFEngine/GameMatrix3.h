#pragma once
#include "GameVector3.h"

class GameMatrix3
{
	friend GameMatrix3 operator * (float scalar, const GameMatrix3 & matrix);
	friend GameMatrix3 operator * (const GameMatrix3 & matrix, float scalar);

	protected:
		GameVector3 row00;
		GameVector3 row01;
		GameVector3 row02;

	public:
		static GameMatrix3 transpose(GameMatrix3 & matrix);

		GameMatrix3();
		GameMatrix3(const GameMatrix3 & matrix);
		GameMatrix3(GameVector3 & row00, GameVector3 & row01, GameVector3 & row02);
		~GameMatrix3();

		GameVector3 & operator [] (size_t index);
		GameMatrix3 operator + (const GameMatrix3 & matrix);
		GameMatrix3 operator * (const GameMatrix3 & matrix);

		void transpose();
};

