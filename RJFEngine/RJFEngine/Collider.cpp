#include "pch.h"
#include "Collider.h"

Collider::Collider(ColliderType colliderType) : ObjectComponent(ComponentType::COLLIDER)
{
	this->colliderType = colliderType;
}

Collider::~Collider()
{
}

ColliderType Collider::GetColliderType() 
{
	return colliderType;
}

Vector3 Collider::GetCenter() 
{
	return center;
}

void Collider::SetCenter(Vector3 center) 
{
	this->center = center;
	
	if (boundingBox) 
	{
		boundingBox->Center = center;
	}
	if (boundingSphere)
	{
		boundingSphere->Center = center;
	}
}

Vector3 Collider::GetSize() 
{
	return size;
}

void Collider::SetSize(Vector3 size)
{
	this->size = size;

	if (boundingBox) 
	{
		boundingBox->Extents = size;
	}
}

float Collider::GetRadius() 
{
	return radius;
}

void Collider::SetRadius(float radius)
{
	this->radius = radius;

	if (boundingSphere)
	{
		boundingSphere->Radius = radius;
	}
}

bool Collider::Intersects(Ray ray, float distance) 
{
	if (colliderType == ColliderType::BOX_COLLIDER) 
	{
		return ray.Intersects(*boundingBox, distance);
	}
	else if (colliderType == ColliderType::SPHERE_COLLIDER) 
	{
		return ray.Intersects(*boundingSphere, distance);
	}

	return false;
}

bool Collider::IsColliding(Collider* other) 
{
	if (this->colliderType == ColliderType::BOX_COLLIDER) 
	{
		if (other->colliderType == ColliderType::BOX_COLLIDER) 
		{
			return this->boundingBox->Intersects(*other->boundingBox);
		}
		else if (other->colliderType == ColliderType::SPHERE_COLLIDER) 
		{
			return this->boundingBox->Intersects(*other->boundingSphere);
		}
	}
	else if (this->colliderType == ColliderType::SPHERE_COLLIDER) 
	{
		if (other->colliderType == ColliderType::BOX_COLLIDER) 
		{
			return this->boundingSphere->Intersects(*other->boundingBox);
		}
		else if (other->colliderType == ColliderType::SPHERE_COLLIDER) 
		{
			return this->boundingSphere->Intersects(*other->boundingSphere);
		}
	}

	return false;
}

void Collider::Transform()
{
	this->center = gameObject->transform->getPosition();

	if (this->boundingBox != nullptr)
	{
		this->boundingBox->Center = this->center;
	}
	if (this->boundingSphere != nullptr)
	{
		this->boundingSphere->Center = this->center;
	}
}

bool Collider::AddIdToCollidingList(int id)
{
	if (!ContainsInCollidingList(id))
	{
		return false;
	}

	collidingIds.insert(id);
	return true;
}

bool Collider::ContainsInCollidingList(int id)
{
	return collidingIds.find(id) != collidingIds.end();
}

bool Collider::RemoveFromCollidingList(int id)
{
	if (!ContainsInCollidingList(id))
	{
		return false;
	}

	hashIterator = collidingIds.begin();
	collidingIds.erase(hashIterator);
	return true;
}