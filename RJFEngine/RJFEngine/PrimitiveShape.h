#pragma once
#include <vector>
#include "pch.h"
#include "GameComponent.h"
#include "CustomMatrix.h"

using std::vector;
using namespace DirectX;

class PrimitiveShape abstract : public GameComponent
{
	protected:
		CustomMatrix vectors;
		float radius;
		CustomMatrix transformations;
		XMVECTORF32 color;

		PrimitiveShape();

	public:
		~PrimitiveShape();

		virtual void translate(float translationX, float translationY, float translationZ, bool resetObjBeforeTransformation);
		virtual void scale(float scaleX, float scaleY, float scaleZ, bool resetObjBeforeTransformation);
		virtual void reflect(bool reflectionX, bool reflectionY, bool reflectionZ, bool resetObjBeforeTransformation);
		virtual void calculateCenter(float &centerX, float &centerY, bool resetObjBeforeTransformation) = 0;
		virtual void rotate(float degrees, Axis axis, bool resetObjBeforeTransformation);

		CustomMatrix getVectors();
		void setVector(int vectorIndex, vector<float> coordinates);
		float getRadius();
		void setRadius(float radius);
		CustomMatrix getTransformations();
		void setTransformations(CustomMatrix transformations);
		XMVECTORF32 getColor();
		void setColor(XMVECTORF32 color);
};

