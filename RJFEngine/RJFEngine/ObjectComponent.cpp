#include "pch.h"
#include "ObjectComponent.h"

using namespace DX;
using namespace DirectX;
using namespace DirectX::SimpleMath;

ObjectComponent::ObjectComponent(ComponentType t) : componentType(t) {
}

ObjectComponent::~ObjectComponent() {
}

const ComponentType ObjectComponent::GetComponentType() {
	return componentType;
}