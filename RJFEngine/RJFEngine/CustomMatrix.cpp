#include "pch.h"
#include <exception>
#include "CustomMatrix.h"

using std::exception;

CustomMatrix::CustomMatrix() {
	rows = 2;
	cols = 2;
	elements.resize(rows, vector<float>(cols));
}

CustomMatrix::CustomMatrix(size_t rows, size_t cols) {
	this->rows = rows;
	this->cols = cols;
	elements.resize(rows, vector<float>(cols));
}

CustomMatrix::~CustomMatrix() {
}

vector<float>& CustomMatrix::operator[](size_t index) {
	return elements.at(index);
}

CustomMatrix CustomMatrix::operator+(const CustomMatrix & matrix) {
	if (this->rows != matrix.rows || this->cols != matrix.cols) {
		throw new exception("Invalid operation.");
	}

	CustomMatrix result(this->rows, this->cols);

	for (size_t row = 0; row < elements.size(); row++) {
		for (size_t col = 0; col < elements.at(row).size(); col++) {
			result[row][col] = elements.at(row).at(col) + matrix.elements.at(row).at(col);
		}
	}

	return result;
}

CustomMatrix CustomMatrix::operator*(const CustomMatrix & matrix) {
	if (this->cols != matrix.rows) {
		throw new exception("Invalid operation.");
	}

	CustomMatrix result(this->rows, matrix.cols);

	for (size_t thisRow = 0; thisRow < this->rows; ++thisRow) {
		for (size_t CustomMatrixCol = 0; CustomMatrixCol < matrix.cols; ++CustomMatrixCol) {
			for (size_t thisCol = 0; thisCol < this->cols; ++thisCol) {
				result[thisRow][CustomMatrixCol] += elements[thisRow][thisCol] * matrix.elements.at(thisCol).at(CustomMatrixCol);
			}
		}
	}

	return result;
}

CustomMatrix CustomMatrix::transpose() {
	CustomMatrix transpose(cols, rows);

	for (size_t row = 0; row < elements.size(); row++) {
		for (size_t col = 0; col < elements.at(row).size(); col++) {
			transpose[col][row] = elements.at(row).at(col);
		}
	}

	return transpose;
}

ostream & operator<<(ostream & output, const CustomMatrix & matrix) {
	for (size_t row = 0; row < matrix.elements.size(); row++) {
		output << "( ";
		for (size_t col = 0; col < matrix.elements.at(row).size(); col++) {
			output << matrix.elements.at(row).at(col) << " ";
		}

		output << ")\n";
	}

	return output;
}

CustomMatrix operator*(float scalar, const CustomMatrix & matrix) {
	CustomMatrix result(matrix.rows, matrix.cols);


	for (size_t row = 0; row < matrix.elements.size(); row++) {
		for (size_t col = 0; col < matrix.elements.at(row).size(); col++) {
			result[row][col] = scalar * matrix.elements.at(row).at(col);
		}
	}

	return result;
}

CustomMatrix operator*(const CustomMatrix & matrix, float scalar) {
	return (scalar * matrix);
}
