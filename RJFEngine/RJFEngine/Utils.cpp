#include "pch.h"
#include "Utils.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

const std::wstring GameUtils::WINDOW_TITLE = L"RJF Engine - ITD Canada Project";
const int GameUtils::FPS = 60;
// The minimum size is 320x200
const int GameUtils::DEFAULT_WIDTH = 1280;
const int GameUtils::DEFAULT_HEIGHT = 720;

// Window Background Color
const XMVECTORF32 GameUtils::DEFAULT_BACKGROUND_COLOR = Colors::CornflowerBlue;

// Camera Constants
const Vector3 GameUtils::DEFAULT_CAMERA_POSITION = Vector3(0.f, 0.f, 4.0f);
const Vector3 GameUtils::DEFAULT_CAMERA_TARGET = Vector3::Zero;
const float GameUtils::NEAR_PLANE = 0.1f;
const float GameUtils::FAR_PLANE = 100.f;

const float MathUtils::PI = 3.1415926535f;
const float MathUtils::degreesToRadians = PI / 180.0f;
const float MathUtils::radiansToDegrees = 180 / PI;
const float MathUtils::infinity = 999.999f;
const float MathUtils::negativeInfinity = -infinity;
const float MathUtils::epsilon = 0.00001f;

float MathUtils::degToRad(float degrees) {
	// R = D*PI / 180;
	return degrees * degreesToRadians;
}

float MathUtils::radToDeg(float radians) {
	// 180R/PI = D
	return radians * radiansToDegrees;
}

float MathUtils::sinDegrees(float angle) {
	return sin(degToRad(angle));
}

float MathUtils::cosDegrees(float angle) {
	return cos(degToRad(angle));
}

float MathUtils::sinRadians(float radians) {
	return sin(radians);
}

float MathUtils::cosRadians(float radians) {
	return cos(radians);
}

//Line* MathUtils::linearEquation(float a, float b) {
//	float x, y, z;
//
//	//Defining P1
//	y = 0;
//	x = (b * -1) / a;
//	z = 0;
//
//	GameVector3 p1 = GameVector3(x, y, z);
//
//	//Defining P2
//	x = 0;
//	y = (a * x) + b;
//
//	GameVector3 p2 = GameVector3(x, y, z);
//
//	return new Line(p1, p2);
//}

int MathUtils::primeFactorization(int number)
{
	int result = 0;

	for (int i = 2; i <= number; i++)
	{
		while (number % i == 0)
		{
			number /= i;
			result += i;
		}
	}

	return result;
}

long MathUtils::permutation(int n, int r)
{
	return factorial(n) / factorial(n - r);
}

int MathUtils::factorial(int number)
{
	if (number == 0 || number == 1)
	{
		return 1;
	}

	if (number == 2)
	{
		return number;
	}

	int n = number;

	for (int i = number; i > 1; i--)
	{
		n *= i - 1;
	}

	return n;
}

long MathUtils::combination(int n, int r)
{
	return factorial(n) / (factorial(n - r) * factorial(r));
}

//float VectorUtils::dotProduct(const GameVector3 & vectorA, const GameVector3 & vectorB)
//{
//	return (vectorA.x * vectorB.x + vectorA.y * vectorB.y + vectorA.z * vectorB.z);
//}

//CustomMatrix MatrixUtils::getIdentityMatrix(size_t matrixSize) {
//	CustomMatrix result(matrixSize, matrixSize);
//
//	for (size_t index = 0; index < matrixSize; index++) {
//		result[index][index] = 1.f;
//	}
//
//	return result;
//}

//CustomMatrix MatrixUtils::getTranslationMatrix(float translationX, float translationY, float translationZ) {
//	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);
//
//	for (size_t index = 0; index < Dimensions::FOUR_DIMENSIONS; index++) {
//		result[index][index] = 1.f;
//	}
//
//	result[Axis::W][Axis::X] = translationX;
//	result[Axis::W][Axis::Y] = translationY;
//	result[Axis::W][Axis::Z] = translationZ;
//
//	return result;
//}

//CustomMatrix MatrixUtils::getScaleMatrix(float scaleX, float scaleY, float scaleZ) {
//	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);
//
//	result[Axis::X][Axis::X] = scaleX;
//	result[Axis::Y][Axis::Y] = scaleY;
//	result[Axis::Z][Axis::Z] = scaleZ;
//	result[Axis::W][Axis::W] = 1.f;
//
//	return result;
//}

//CustomMatrix MatrixUtils::getReflectionMatrix(bool reflectionX, bool reflectionY, bool reflectionZ) {
//	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);
//
//	result[Axis::X][Axis::X] = reflectionX ? -1.f : 1.f;
//	result[Axis::Y][Axis::Y] = reflectionY ? -1.f : 1.f;
//	result[Axis::Z][Axis::Z] = reflectionZ ? -1.f : 1.f;
//	result[Axis::W][Axis::W] = 1.f;
//
//	return result;
//}

//CustomMatrix MatrixUtils::getRotationMatrix(float degrees, Axis axis) {
//	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);
//	float radians = MathUtils::degToRad(degrees);
//
//	result[0][Axis::X] = (Axis::X == axis ? 1.f : MathUtils::cosRadians(radians));
//	result[0][Axis::Y] = (Axis::Z == axis ? MathUtils::sinRadians(radians) : 0.f);
//	result[0][Axis::Z] = (Axis::Y == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);
//
//	result[1][Axis::X] = (Axis::Z == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);
//	result[1][Axis::Y] = (Axis::Y == axis ? 1.f : MathUtils::cosRadians(radians));
//	result[1][Axis::Z] = (Axis::X == axis ? MathUtils::sinRadians(radians) : 0.f);
//
//	result[2][Axis::X] = (Axis::Y == axis ? MathUtils::sinRadians(radians) : 0.f);
//	result[2][Axis::Y] = (Axis::X == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);
//	result[2][Axis::Z] = (Axis::Z == axis ? 1.f : MathUtils::cosRadians(radians));
//
//	result[3][Axis::W] = 1.f;
//
//	return result;
//}