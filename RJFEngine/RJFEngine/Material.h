#pragma once

enum WrapMode {
	LINEAR_WRAP,
	ANISOTROPIC_WRAP,
	POINT_WRAP
};

class Material : public ObjectComponent
{
	private:
		// Texture Effect
		std::unique_ptr<BasicEffect> effect;

		// Texture
		Microsoft::WRL::ComPtr<ID3D12Resource> textureResource;

	public:
		size_t index;
		std::wstring texture;
		bool lighting;
		WrapMode wrapMode;

		Material(size_t index, std::wstring textureName, bool lighting, WrapMode wrapMode = WrapMode::LINEAR_WRAP);
		~Material();

		void applyEffects(ID3D12GraphicsCommandList * commandList);
		void setEffect(BasicEffect * effect);
		BasicEffect * getEffect();
		ID3D12Resource ** clearTextureResource();
		ID3D12Resource * getTextureResource();
};