#pragma once

#include <string>
#include <vector>
#include <iterator>
#include "ObjectComponent.h"

#include "Transform.h"
#include "Primitive3D.h"
#include "Material.h"
#include "Collider.h"
#include "RigidBody.h"
#include "Behaviour.h"

using std::string;
using std::vector;
using std::iterator;
using std::advance;

class GameObject {
public:
	int id;
	bool enabled = true;
	string name;
	bool staticObject = false;

	vector<ObjectComponent*> components;
	Transform* transform = nullptr;
	Primitive3D* primitive3D = nullptr;
	Material* material = nullptr;
	Collider* collider = nullptr;
	RigidBody* rigidBody = nullptr;
	Behaviour* behaviour = nullptr;

	GameObject(string name);
	~GameObject();

	int GetComponentIndex(ComponentType type);
	bool HasComponent(ComponentType type);
	void AddComponent(ObjectComponent* component);
	void RemoveComponent(ComponentType type);
	ObjectComponent* GetComponentByType(ComponentType type);

	bool HasValidTransform();
	bool HasValidPrimitive3D();
	bool HasValidCollider();
	bool HasValidRigidBody();
	bool HasValidBehaviour();
	bool HasValidMaterial();

	Vector3 GetPosition();
};