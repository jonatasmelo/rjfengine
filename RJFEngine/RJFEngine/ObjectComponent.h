#pragma once

enum ComponentType {
	TRANSFORM = 0,
	PRIMITIVE_3D = 1,
	MESH = 2,
	FONT = 3,
	COLLIDER = 4,
	RIGID_BODY = 5,
	SPRITE = 6,
	AUDIO_SOURCE = 7,
	BEHAVIOUR = 8,
	MATERIAL = 9,
	CAMERA = 10
};

class GameObject;

class ObjectComponent abstract
{
protected:
	const ComponentType componentType;

public:
	GameObject* gameObject = nullptr;
	bool enabled;

	ObjectComponent(ComponentType type);
	~ObjectComponent();

	bool operator==(ObjectComponent* other)
	{
		return other->componentType == this->componentType;
	}

	bool operator!=(ObjectComponent* other)
	{
		return other->componentType != this->componentType;
	}

	const ComponentType GetComponentType();
};