#pragma once
#include <vector>

using std::vector;

class GameTerrain
{
	private:
		float size;
		float max;
		vector<float> map;

	public:
		GameTerrain();
		GameTerrain(float detail);
		~GameTerrain();

		float get(float x, float y);
		void set(float x, float y, float val);
		void generate(float roughness);
		void divide(float size, float roughness);
		float average(vector<float> & values);
		void square(float x, float y, float size, float offset);
		void diamond(float x, float y, float size, float offset);
		void draw(Graphics * graphics);
};

