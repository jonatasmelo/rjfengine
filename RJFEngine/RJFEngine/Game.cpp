//
// Game.cpp
//

#include "pch.h"
#include "Game.h"

extern void ExitGame();

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Game::Game()
{
    m_deviceResources = std::make_unique<DX::DeviceResources>();
    m_deviceResources->RegisterDeviceNotify(this);

	transformProcessor = new TransformProcessor();
	rigidBodyProcessor = new RigidBodyProcessor();
	behaviourProcessor = new BehaviourProcessor();
	collisionProcessor = new CollisionProcessor();
}

Game::~Game()
{
    if (m_deviceResources)
    {
        m_deviceResources->WaitForGpu();
    }
}

// Initialize the Direct3D resources required to run.
void Game::Initialize(HWND window, int width, int height)
{
	setBackgroundColor(GameUtils::DEFAULT_BACKGROUND_COLOR);

    m_deviceResources->SetWindow(window, width, height);
	graphics = std::make_unique<Graphics>(m_deviceResources.get());

    m_deviceResources->CreateDeviceResources();
    CreateDeviceDependentResources();

    m_deviceResources->CreateWindowSizeDependentResources();
    CreateWindowSizeDependentResources();

	// Initialize all resources
	ResourceManager::createInstance(m_deviceResources.get());

    // TODO: Change the timer settings if you want something other than the default variable timestep mode.
    // e.g. for 60 FPS fixed timestep update logic, call:
    m_timer.SetFixedTimeStep(true);
    m_timer.SetTargetElapsedSeconds(1.0 / GameUtils::FPS);

	// Initializing the Input class
	Input::getInstance()->setWindowReference(window);

	// Seed based on time for random numbers generation
	srand(time(NULL));
}

#pragma region Frame Update
// Executes the basic game loop.
void Game::Tick()
{
    m_timer.Tick([&]()
    {
        Update(m_timer);
    });

    Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
    PIXBeginEvent(PIX_COLOR_DEFAULT, L"Update");

	Time::UpdateTimer(timer);
	updatePhase(timer);

	vector<GameObject *> * objects = getObjectList();

	if (objects != nullptr) {
		for (int i = 0; i < objects->size(); i++)
		{
			GameObject* objA = objects->at(i);

			if (objA->enabled)
			{
				transformProcessor->Execute(objA);

				rigidBodyProcessor->Execute(objA);

				behaviourProcessor->Execute(objA);

				collisionProcessor->updateObjectList(objects);
				collisionProcessor->Execute(objA);
			}
		}
	}

	graphics->updateViewMatrix();
	graphics->updateProjectionMatrix();

	ResourceManager::updateView(camera->getView());
	ResourceManager::updateProjection(camera->getProjection());

    PIXEndEvent();
}
#pragma endregion

#pragma region Frame Render
// Draws the scene.
void Game::Render()
{
    // Don't try to render anything before the first Update.
    if (m_timer.GetFrameCount() == 0)
    {
        return;
    }

    // Prepare the command list to render a new frame.
    m_deviceResources->Prepare();
    Clear();

    auto commandList = m_deviceResources->GetCommandList();
    PIXBeginEvent(commandList, PIX_COLOR_DEFAULT, L"Render");

    // TODO: Add your rendering code here.
	ID3D12DescriptorHeap * heaps[] = { resourceDescriptors->Heap(), commonStates->Heap() };
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);

	renderPhase();

	vector<GameObject *> * objects = getObjectList();

	if (objects != nullptr) {
		for (int i = 0; i < objects->size(); i++) {
			GameObject* objA = objects->at(i);

			if (objA->enabled) {
				{//PRIMITIVE_SHAPE
					if (objA->primitive3D != nullptr) {
						if (objA->material != nullptr) {
							objA->material->getEffect()->SetWorld(objA->transform->objectMatrix * graphics->world);
							objA->material->getEffect()->SetView(camera->getView());
							objA->material->getEffect()->SetProjection(camera->getProjection());
							objA->material->applyEffects(commandList);
						}
						else {
							BasicEffect * fx = ResourceManager::getFXPrimitive();
							fx->SetWorld(objA->transform->objectMatrix * graphics->world);
							fx->Apply(commandList);
						}

						objA->primitive3D->GetShape()->Draw(commandList);
					}
				}
			}
		}
	}

    PIXEndEvent(commandList);

    // Show the new frame.
    PIXBeginEvent(m_deviceResources->GetCommandQueue(), PIX_COLOR_DEFAULT, L"Present");
    m_deviceResources->Present();
	m_graphicsMemory->Commit(m_deviceResources->GetCommandQueue());
    PIXEndEvent(m_deviceResources->GetCommandQueue());
}

// Helper method to clear the back buffers.
void Game::Clear()
{
    auto commandList = m_deviceResources->GetCommandList();
    PIXBeginEvent(commandList, PIX_COLOR_DEFAULT, L"Clear");

    // Clear the views.
    auto rtvDescriptor = m_deviceResources->GetRenderTargetView();
    auto dsvDescriptor = m_deviceResources->GetDepthStencilView();

    commandList->OMSetRenderTargets(1, &rtvDescriptor, FALSE, &dsvDescriptor);
    commandList->ClearRenderTargetView(rtvDescriptor, backgroundColor, 0, nullptr);
    commandList->ClearDepthStencilView(dsvDescriptor, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

    // Set the viewport and scissor rect.
    auto viewport = m_deviceResources->GetScreenViewport();
    auto scissorRect = m_deviceResources->GetScissorRect();
    commandList->RSSetViewports(1, &viewport);
    commandList->RSSetScissorRects(1, &scissorRect);

    PIXEndEvent(commandList);
}
#pragma endregion

#pragma region Message Handlers
// Message handlers
void Game::OnActivated()
{
    // TODO: Game is becoming active window.
}

void Game::OnDeactivated()
{
    // TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
    // TODO: Game is being power-suspended (or minimized).
}

void Game::OnResuming()
{
    m_timer.ResetElapsedTime();

    // TODO: Game is being power-resumed (or returning from minimize).
}

void Game::OnWindowSizeChanged(int width, int height)
{
    if (!m_deviceResources->WindowSizeChanged(width, height))
        return;

    CreateWindowSizeDependentResources();

    // TODO: Game window is being resized.
}

// Properties
void Game::GetDefaultSize(int& width, int& height) const
{
    // TODO: Change to desired default window size (note minimum size is 320x200).
    width = std::max(GameUtils::DEFAULT_WIDTH, 320);
    height = std::max(GameUtils::DEFAULT_HEIGHT, 200);
}
#pragma endregion

#pragma region Direct3D Resources
// These are the resources that depend on the device.
void Game::CreateDeviceDependentResources()
{
    auto device = m_deviceResources->GetD3DDevice();

    // TODO: Initialize device dependent objects here (independent of window size).
	m_graphicsMemory = std::make_unique<GraphicsMemory>(device);
	
	auto size = m_deviceResources->GetOutputSize();
	camera = std::make_unique<Camera>(XM_PI * 0.25f, float(size.right), float(size.bottom), GameUtils::NEAR_PLANE, GameUtils::FAR_PLANE, GameUtils::DEFAULT_CAMERA_POSITION, GameUtils::DEFAULT_CAMERA_TARGET);
	
	graphics->initDeviceDependentResources(camera.get());
	materials = std::make_unique<vector<Material*>>();

	initDeviceDependentResources();

	resourceDescriptors = std::make_unique<DescriptorHeap>(device,
		D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
		D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE,
		materials->size()
		);

	commonStates = std::make_unique<CommonStates>(device);

	// Upload Resources
	{
		ResourceUploadBatch resourceUpload(device);
		resourceUpload.Begin();

		RenderTargetState rtState(DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_FORMAT_D32_FLOAT);
		rtState.sampleDesc.Count = 4; // <--- 4x MSAA

		EffectPipelineStateDescription psd(
			&GeometricPrimitive::VertexType::InputLayout,
			CommonStates::Opaque,
			CommonStates::DepthDefault,
			CommonStates::CullNone,
			rtState);

		for (size_t i = 0; i < materials->size(); i++) {
			Material * material = materials->at(i);

			DX::ThrowIfFailed(
				CreateDDSTextureFromFile(device, resourceUpload, material->texture.c_str(), material->clearTextureResource())
			);

			CreateShaderResourceView(device, material->getTextureResource(), resourceDescriptors->GetCpuHandle(material->index));

			int flags = EffectFlags::Texture;
			if (material->lighting) {
				flags = flags | EffectFlags::Lighting;
			}

			BasicEffect * effect = new BasicEffect(device, flags, psd);
			if (material->lighting) {
				effect->EnableDefaultLighting();
				effect->SetLightEnabled(0, true);
				effect->SetLightDiffuseColor(0, Colors::White);
				effect->SetLightSpecularColor(0, Colors::White);
				effect->SetLightDirection(0, Vector3(-0.4f, -0.4f, 0.4f));
			}

			D3D12_GPU_DESCRIPTOR_HANDLE wrapMode;
			switch (material->wrapMode)
			{
			case WrapMode::LINEAR_WRAP:
				wrapMode = commonStates->LinearWrap();
				break;

			case WrapMode::ANISOTROPIC_WRAP:
				wrapMode = commonStates->AnisotropicWrap();
				break;

			case WrapMode::POINT_WRAP:
				wrapMode = commonStates->PointWrap();
				break;

			default:
				wrapMode = commonStates->LinearWrap();
			}
			effect->SetTexture(resourceDescriptors->GetGpuHandle(material->index), wrapMode);

			material->setEffect(effect);
		}

		// Upload the resources to the GPU
		auto uploadResourcesFinished = resourceUpload.End(m_deviceResources->GetCommandQueue());
		// Wait for the command list to finish executing
		m_deviceResources->WaitForGpu();
		// Wait for the upload thread to terminate
		uploadResourcesFinished.wait();
	}
}

// Allocate all memory resources that change on a window SizeChanged event.
void Game::CreateWindowSizeDependentResources()
{
    // TODO: Initialize windows-size dependent objects here.
	if (camera == nullptr)
	{
		CreateCamera("Camera1", Vector3(0, 2.0f, 0));
	}

	graphics->initWindowSizeDependentResources();
	initWindowSizeDependentResources();
}

void Game::setBackgroundColor(DirectX::XMVECTORF32 newColor)
{
	backgroundColor = newColor;
}

void Game::OnDeviceLost()
{
    // TODO: Add Direct3D resource cleanup here.
	resetResources();

	resourceDescriptors.reset();
	commonStates.reset();
	camera.reset();
	materials.reset();

	graphics->resetResources();
	graphics.reset();
	m_graphicsMemory.reset();
}

void Game::OnDeviceRestored()
{
    CreateDeviceDependentResources();

    CreateWindowSizeDependentResources();
}

//GameObject* Game::Instantiate(string name, Quaternion rotation, Vector3 position, bool staticObject)
//{
//	GameObject* object = new GameObject(name);
//	object->id = currentObjectID;
//	object->transform->Translate(position);
//	object->transform->Rotate(rotation);
//	object->staticObject = staticObject;
//	currentObjectID++;
//
//	objects.push_back(object);
//
//	return object;
//}

Camera* Game::CreateCamera(string name, Vector3 position, Quaternion rotation, Vector3 target)
{
	auto size = m_deviceResources->GetOutputSize();

	Camera* cam = new Camera(XM_PI * 0.25f, float(size.right), float(size.bottom), 0.1f, 100.0f, position, target);
	cam->id = currentCameraID;
	currentCameraID++;

	cameras.push_back(cam);

	if (camera == nullptr) {
		camera = std::unique_ptr<Camera>(cam);
		graphics->updateViewMatrix();
		graphics->updateProjectionMatrix();
	}

	return cam;
}

Collider* collider = nullptr;

bool Game::Intersects(Ray ray, GameObject* obj, float distance) {
	if (obj == nullptr) {
		return false;
	}

	collider = (Collider*)obj->GetComponentByType(ComponentType::COLLIDER);

	if (collider == nullptr) {
		return false;
	}

	return collider->Intersects(ray, distance);
}

Material * Game::CreateMaterial(std::wstring textureName, bool lighting, WrapMode wrapMode)
{
	Material * material = new Material(materials->size(), textureName, lighting, wrapMode);
	materials->push_back(material);
	return material;
}

#pragma endregion
