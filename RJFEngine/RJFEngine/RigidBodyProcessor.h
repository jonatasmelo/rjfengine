#pragma once

#include "Processor.h"

class RigidBodyProcessor : Processor
{

public:
	RigidBodyProcessor();
	~RigidBodyProcessor();

	void Execute(GameObject* objA) override;
};