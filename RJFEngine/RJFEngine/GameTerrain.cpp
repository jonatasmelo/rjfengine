#include "pch.h"
#include "GameTerrain.h"

GameTerrain::GameTerrain()
{
}

GameTerrain::GameTerrain(float detail)
{
	size = pow(4, detail) + 1;
	max = size - 1;
	map.clear();
	map.resize(size * size);
}

GameTerrain::~GameTerrain()
{
}

float GameTerrain::get(float x, float y)
{
	if (x < 0.f || x > max || y < 0.f || y > max) {
		return -1.f;
	}

	return map.at(x + size * y);
}

void GameTerrain::set(float x, float y, float val)
{
	map.at(x + size * y) = val;
}

void GameTerrain::generate(float roughness)
{
	set(0, 0, max);
	set(max, 0, max / 2.f);
	set(max, max, 0);
	set(0, max, max / 2.f);

	divide(max, roughness);
}

void GameTerrain::divide(float size, float roughness)
{
	float x, y;
	float half = size / 2.f;
	float scale = roughness * size;

	if (half < 1.f) {
		return;
	}

	for (y = half; y < max; y += size) {
		for (x = half; x < max; x += size) {
			square(x, y, half, rand() * scale * 2 - scale);
		}
	}

	for (y = 0; y < max; y += half) {
		x = fmod((y + half), size);
		for (; x <= max; x += size) {
			diamond(x, y, half, rand() * scale * 2 - scale);
		}
	}
}

float GameTerrain::average(vector<float>& values)
{
	float sum = 0.f;

	for (size_t i = 0; i < values.size(); i++)
	{
		sum += values.at(i);
	}

	return sum / values.size();
}

void GameTerrain::square(float x, float y, float size, float offset)
{
	vector<float> values = {
		get(x - size, y - size),
		get(x + size, y - size),
		get(x + size, y + size),
		get(x - size, y + size)
	};

	set(x, y, average(values) + offset);
}

void GameTerrain::diamond(float x, float y, float size, float offset)
{
	vector<float> values = {
		get(x, y - size),
		get(x + size, y),
		get(x, y + size),
		get(x - size, y)
	};

	set(x, y, average(values) + offset);
}

void GameTerrain::draw(Graphics * graphics)
{
	Vector3 * vectorList = new Vector3[size * size];

	for (float y = 0; y < size; y++) {
		for (float x = 0; x < size; x++) {
			float height = get(x, y);
			
			vectorList[static_cast<int>(x + size * y)] = Vector3(x, height, y);
		}
	}

	//graphics->drawVertices(vectorList, static_cast<int>(size*size), Colors::LightGray);
}
