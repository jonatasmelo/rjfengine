#pragma once

#include "Game.h"

class Time
{
	friend class Game;

private:
	Time();

protected:	
	static void UpdateTimer(const DX::StepTimer& stepTimer);

public:
	static float elpasedTime;
};