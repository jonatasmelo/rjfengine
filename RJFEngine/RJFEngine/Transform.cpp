#include "pch.h"
#include "Transform.h"

Transform::Transform() : ObjectComponent(ComponentType::TRANSFORM)
{
}

Transform::~Transform()
{
}

void Transform::Translate(Vector3 axis)
{
	objectMatrix.Translation(axis);
}

void Transform::Translation(Vector3 axis, float speed)
{
	Vector3 position = getPosition();
	position += (axis * speed);
	objectMatrix.Translation(position);
}

void Transform::TranslationUp(float speed)
{
	Translation(Vector3::UnitY, speed);
}

void Transform::TranslationDown(float speed)
{
	Translation(Vector3::UnitY, -speed);
}

void Transform::TranslationRight(float speed)
{
	Translation(Vector3::UnitX, speed);
}

void Transform::TranslationLeft(float speed)
{
	Translation(Vector3::UnitX, -speed);
}

void Transform::TranslationBack(float speed)
{
	Translation(Vector3::UnitZ, -speed);
}

void Transform::TranslationForth(float speed)
{
	Translation(Vector3::UnitZ, speed);
}

void Transform::Rotate(Quaternion rotation)
{
	objectMatrix *= Matrix::CreateFromQuaternion(rotation);
}

void Transform::Rotate(Vector3 axis, float angle)
{
	objectMatrix *= Matrix::CreateFromAxisAngle(axis, angle);
}

void Transform::Rotation(Vector3 axis, float speed)
{
	// TODO - Check if this is necessary
	rotationSpeed += speed;
	objectMatrix = objectMatrix.CreateFromAxisAngle(axis, rotationSpeed);
}

void Transform::RotationX(Vector3 origin, float speed, bool lookAtOrigin)
{
	Orbit(origin, speed, Vector3::UnitX, lookAtOrigin);
}

void Transform::RotationY(Vector3 origin, float speed, bool lookAtOrigin)
{
	Orbit(origin, speed, Vector3::UnitY, lookAtOrigin);
}

void Transform::RotationZ(Vector3 origin, float speed, bool lookAtOrigin)
{
	Orbit(origin, speed, Vector3::UnitZ, lookAtOrigin);
}

void Transform::Orbit(Vector3 origin, float speed, Vector3 axis, bool lookAtOrigin)
{
	Translate(getPosition() - origin);
	objectMatrix *= Matrix::CreateFromAxisAngle(axis, speed);
	Translate(getPosition() + origin);

	if (lookAtOrigin) {
		objectMatrix = Matrix::CreateLookAt(getPosition(), origin, Camera::UP_DIRECTION);
	}
}

void Transform::Scale(Vector3 axis) 
{
	// TODO
	//objectMatrix.CreateScale(axis);
}

void Transform::ScaleUpAll(float speed)
{
	// TODO
	//objectMatrix.CreateScale(scale);
}

void Transform::ScaleDownAll(float speed)
{
	// TODO
	//objectMatrix.CreateScale(scale);
}

Vector3 Transform::getPosition()
{
	return Vector3(objectMatrix._41, objectMatrix._42, objectMatrix._43);
}
