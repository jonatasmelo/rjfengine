//
// Game.h
//

#pragma once

#include <string>
#include <vector>
#include "DeviceResources.h"
#include "StepTimer.h"
#include "Graphics.h"
#include "TransformProcessor.h"
#include "RigidBodyProcessor.h"
#include "BehaviourProcessor.h"
#include "CollisionProcessor.h"
#include "Material.h"

using std::string;
using std::vector;

// A basic game implementation that creates a D3D12 device and
// provides a game loop.
class Game abstract : public DX::IDeviceNotify
{
public:
    Game();
    ~Game();

    // Initialization and management
    void Initialize(HWND window, int width, int height);

    // Basic game loop
    void Tick();

    // IDeviceNotify
    virtual void OnDeviceLost() override;
    virtual void OnDeviceRestored() override;

    // Messages
    void OnActivated();
    void OnDeactivated();
    void OnSuspending();
    void OnResuming();
    void OnWindowSizeChanged(int width, int height);

    // Properties
    void GetDefaultSize( int& width, int& height ) const;

private:
	//AUX
	int currentObjectID = 0;
	int currentCameraID = 0;

	//ComponentProcessors
	TransformProcessor* transformProcessor = nullptr;
	RigidBodyProcessor* rigidBodyProcessor = nullptr;
	BehaviourProcessor* behaviourProcessor = nullptr;
	CollisionProcessor* collisionProcessor = nullptr;
	
    void Update(DX::StepTimer const& timer);
    void Render();

    void Clear();

    void CreateDeviceDependentResources();
    void CreateWindowSizeDependentResources();

    // Device resources.
    std::unique_ptr<DX::DeviceResources>    m_deviceResources;

    // Rendering loop timer.
    DX::StepTimer                           m_timer;

	// DirectXTK objects
	std::unique_ptr<DirectX::GraphicsMemory> m_graphicsMemory;

	// Engine objects
	DirectX::XMVECTORF32 backgroundColor;

protected:
	// DirectXTK Objects
	std::unique_ptr<DescriptorHeap> resourceDescriptors;
	std::unique_ptr<CommonStates> commonStates;

	// Engine Objects
	std::unique_ptr<Graphics> graphics;
	std::unique_ptr<Camera> camera;

	vector<Camera*> cameras;
	std::unique_ptr<vector<Material*>> materials;

	Camera* CreateCamera(string name, Vector3 position, Quaternion rotation = Quaternion::Identity, Vector3 target = Vector3::Zero);
	bool Intersects(Ray ray, GameObject* obj, float distance);
	Material * CreateMaterial(std::wstring textureName, bool lighting, WrapMode wrapMode = WrapMode::LINEAR_WRAP);

	virtual void initDeviceDependentResources() = 0;
	virtual void initWindowSizeDependentResources() = 0;
	virtual void updatePhase(DX::StepTimer const& timer) = 0;
	virtual void renderPhase() = 0;
	virtual void resetResources() = 0;
	virtual vector<GameObject *> * getObjectList() = 0;

	void setBackgroundColor(DirectX::XMVECTORF32 newColor);
};