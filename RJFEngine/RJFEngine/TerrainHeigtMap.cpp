#include "pch.h"
#include "TerrainHeigtMap.h"

size_t TerrainHeigtMap::index(size_t x, size_t y) const
{
	const size_t x_wrapped = x % width;
	const size_t y_wrapped = y % width;

	return y_wrapped * width + x_wrapped;
}

void TerrainHeigtMap::setLevelOfDetail(size_t value)
{
	levelOfDetail = value;
	width = static_cast<size_t> (std::pow(2, levelOfDetail) + 1);
	data.resize(width * width);
}

TerrainHeigtMap::TerrainHeigtMap(size_t getLevelOfDetail)
{
	setLevelOfDetail(getLevelOfDetail);
}

TerrainHeigtMap::~TerrainHeigtMap()
{
}

float TerrainHeigtMap::at(size_t x, size_t y) const
{
	return data.at(index(x, y));
}

float & TerrainHeigtMap::at(size_t x, size_t y)
{
	return data.at(index(x, y));
}

size_t TerrainHeigtMap::getLevelOfDetail() const
{
	return levelOfDetail;
}

size_t TerrainHeigtMap::getWidth() const
{
	return width;
}

size_t TerrainHeigtMap::getHeight() const
{
	return width;
}

size_t TerrainHeigtMap::size() const
{
	return data.size();
}

const float * TerrainHeigtMap::getData() const
{
	return data.data();
}
