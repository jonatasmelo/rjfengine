#pragma once

class SphereCollider : public Collider
{
public:
	SphereCollider();
	~SphereCollider();

	BoundingSphere* GetBounds();
};