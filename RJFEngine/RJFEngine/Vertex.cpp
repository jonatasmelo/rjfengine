#include "pch.h"
#include "Vertex.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

Vertex::Vertex()
{
	init();
}

Vertex::Vertex(const Vertex & vertex)
{
	vectors = vertex.vectors;
	transformations = vertex.transformations;
	color = vertex.color;
}

Vertex::Vertex(Vector3 point, float size = 0.1f)
{
	this->point = point;
	this->size = size;
	this->boxCollider.Center = point;
	this->boxCollider.Extents = Vector3(size);
}

Vertex::~Vertex()
{
}

void Vertex::init() {
	vectors = CustomMatrix(1, 1);
//	transformations = MatrixUtils::getIdentityMatrix(1);
	color = Colors::Red;
}

void Vertex::update() {
}

void Vertex::draw(Graphics * graphics) {
	CustomMatrix transformedVectors = vectors * transformations;
//	graphics->drawVertex(point, size, color);
}

void Vertex::destroy() {
}

void Vertex::calculateCenter(float & centerX, float & centerY, bool resetObjBeforeTransformation) {
	CustomMatrix transformedVectors = vectors;

	if (!resetObjBeforeTransformation) {
		transformedVectors = transformedVectors * transformations;
	}

	centerX = (transformedVectors[CustomRectangle::UPPER_LEFT][Axis::X] + transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::X]) / 2;
	centerY = (transformedVectors[CustomRectangle::UPPER_LEFT][Axis::Y] + transformedVectors[CustomRectangle::LOWER_RIGHT][Axis::Y]) / 2;
}

void Vertex::setPoint(Vector3 point)
{
	this->point = point;
	this->boxCollider.Center = point;
	this->boxCollider.Extents = Vector3(size);
}
