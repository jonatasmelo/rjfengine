#include "pch.h"
#include "Primitive3D.h"

Primitive3D::Primitive3D(Primitive3DType primitiveType, float size, size_t tesselation) : ObjectComponent(ComponentType::PRIMITIVE_3D), primitiveType(primitiveType)
{
	this->size = size;

	switch (primitiveType)
	{
	case Primitive3DType::CUBE:
		this->shape = GeometricPrimitive::CreateCube(size);
		break;
	case Primitive3DType::SPHERE:
		this->shape = GeometricPrimitive::CreateSphere(size, tesselation);
		break;
	case Primitive3DType::TEAPOT:
		this->shape = GeometricPrimitive::CreateTeapot(size, tesselation);
		break;
	}
}

Primitive3D::Primitive3D(Primitive3DType primitiveType, Vector3 area) : ObjectComponent(ComponentType::PRIMITIVE_3D), primitiveType(primitiveType)
{
	this->area = area;

	switch (primitiveType)
	{
	case Primitive3DType::BOX:
		this->shape = GeometricPrimitive::CreateBox(area, true, false);
		break;
	}
}

Primitive3D::~Primitive3D()
{
}

GeometricPrimitive* Primitive3D::GetShape()
{
	if (shape == nullptr) {
		return nullptr;
	}

	return shape.get();
}