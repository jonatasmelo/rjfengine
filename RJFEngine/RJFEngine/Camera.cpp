#include "pch.h"
#include "Camera.h"

const Vector3 Camera::UP_DIRECTION = Vector3(0.f, 1.f, 0.f);

Camera::Camera(float fov, float width, float height, float nearPlane, float farPlane, Vector3 position, Vector3 target)
{
	this->screenWidth = width;
	this->screenHeight = height;

	this->fov = fov;
	this->aspectRatio = width / height;
	// For portrait or snapped view
	if (aspectRatio < 1.f) {
		fov *= 2.f;
	}

	this->nearPlane = nearPlane;
	this->farPlane = farPlane;
	
	transform = new Transform();
	transform->Translate(position);

	this->projection = Matrix::CreatePerspectiveFieldOfView(fov, aspectRatio, nearPlane, farPlane);
	this->target = target;
}

Camera::~Camera()
{
}

Matrix Camera::getProjection()
{
	return projection;
}

Matrix Camera::getView()
{
	return Matrix::CreateLookAt(transform->getPosition(), target, Camera::UP_DIRECTION);
}

void Camera::setScreenSize(float width, float height)
{
	float oldAspectRatio = this->aspectRatio;

	this->screenWidth = width;
	this->screenHeight = height;
	this->aspectRatio = width / height;

	// For portrait or snapped view
	if (aspectRatio < 1.f && oldAspectRatio >= 1.f) {
		this->fov *= 2.f;
	}
	else if (oldAspectRatio < 1.f && aspectRatio >= 1.f) {
		this->fov /= 2.f;
	}

	this->projection = Matrix::CreatePerspectiveFieldOfView(fov, aspectRatio, nearPlane, farPlane);
}

void Camera::setTarget(Vector3 target)
{
	this->target = target;
}

Ray Camera::screenToWorldRay(float screenX, float screenY, Matrix world)
{
	Vector3 nearSource = Vector3(screenX, screenY, 0.f);
	Vector3 farSource = Vector3(screenX, screenY, 1.f);

	Vector3 nearPoint = XMVector3Unproject(nearSource, 0, 0, screenWidth, screenHeight, nearPlane, farPlane, projection, getView(), world);
	Vector3 farPoint = XMVector3Unproject(farSource, 0, 0, screenWidth, screenHeight, nearPlane, farPlane, projection, getView(), world);

	Vector3 direction = farPoint - nearPoint;
	direction.Normalize();

	Ray response;
	response.position = nearPoint;
	response.direction = direction;

	return response;
}

Vector3 Camera::screenToWorldPoint(float screenX, float screenY)
{
	float vx = screenX - (screenWidth / 2);
	float vy = screenY - (screenHeight / 2);

	return Vector3(vx, vy, 1);
}

void Camera::Translate(Vector3 axis)
{
	this->transform->Translate(axis);
}

void Camera::Translation(Vector3 axis, float speed)
{
	this->transform->Translation(axis, speed);
}

void Camera::TranslationUp(float speed)
{
	this->transform->TranslationUp(speed);
}

void Camera::TranslationDown(float speed)
{
	this->transform->TranslationDown(speed);
}

void Camera::TranslationRight(float speed)
{
	this->transform->TranslationRight(speed);
}

void Camera::TranslationLeft(float speed)
{
	this->transform->TranslationLeft(speed);
}

void Camera::TranslationBack(float speed)
{
	this->transform->TranslationBack(speed);
}

void Camera::TranslationForth(float speed)
{
	this->transform->TranslationForth(speed);
}

void Camera::Rotate(Quaternion rotation)
{
	this->transform->Rotate(rotation);
}

void Camera::Rotate(Vector3 axis, float angle)
{
	this->transform->Rotate(axis, angle);
}

void Camera::Rotation(Vector3 axis, float speed)
{
	this->transform->Rotation(axis, speed);
}

void Camera::LookAt(Vector3 target)
{
	this->target = target;
}

void Camera::LookAt(GameObject* obj)
{
	LookAt(obj->GetPosition());
}