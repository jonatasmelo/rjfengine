#pragma once

#include "ObjectComponent.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Transform : public ObjectComponent
{
private: 
	float rotationSpeed = 0.0f;

public:
	Transform();
	~Transform();

	Matrix objectMatrix;

	void Translate(Vector3 axis);
	void Translation(Vector3 axis, float speed);
	void TranslationUp(float speed);
	void TranslationDown(float speed);
	void TranslationLeft(float speed);
	void TranslationRight(float speed);
	void TranslationBack(float speed);
	void TranslationForth(float speed);

	void Rotate(Quaternion rotation);
	void Rotate(Vector3 axis, float angle);	
	void Rotation(Vector3 axis, float speed);
	void RotationX(Vector3 origin, float speed, bool lookAtOrigin = false);
	void RotationY(Vector3 origin, float speed, bool lookAtOrigin = false);
	void RotationZ(Vector3 origin, float speed, bool lookAtOrigin = false);
	void Orbit(Vector3 origin, float speed, Vector3 axis, bool lookAtOrigin = false);

	void Scale(Vector3 axis);
	void ScaleUpAll(float speed);
	void ScaleDownAll(float speed);

	Vector3 getPosition();
};