#include "pch.h"
#include "TransformProcessor.h"

TransformProcessor::TransformProcessor()
{
}

TransformProcessor::~TransformProcessor()
{
}

void TransformProcessor::Execute(GameObject* agentA)
{
	if (agentA != nullptr)
	{
		agentA->transform->Translate(agentA->GetPosition());
	}
}