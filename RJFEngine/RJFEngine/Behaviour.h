#pragma once

class Behaviour abstract : public ObjectComponent
{
public:
	Behaviour();
	~Behaviour();

	virtual void OnStart();
	virtual void OnCollisionEnter(GameObject* other);
	virtual void OnCollisionExit(GameObject* other);
	virtual void OnCollisionStay(GameObject* other);
	virtual void OnUpdate();
	virtual void OnRender();
	virtual void OnDestroy();
};