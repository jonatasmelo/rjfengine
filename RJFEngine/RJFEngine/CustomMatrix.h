#pragma once
#include <vector>
#include <iostream>

using std::vector;
using std::ostream;

class CustomMatrix
{
	friend ostream & operator << (ostream & output, const CustomMatrix & matrix);
	friend CustomMatrix operator * (float scalar, const CustomMatrix & matrix);
	friend CustomMatrix operator * (const CustomMatrix & matrix, float scalar);

	private:
		vector<vector<float>> elements;
		size_t rows;
		size_t cols;

	public:
		CustomMatrix();
		CustomMatrix(size_t rows, size_t cols);
		~CustomMatrix();

		vector<float>& operator[](size_t index);
		CustomMatrix operator + (const CustomMatrix & matrix);
		CustomMatrix operator * (const CustomMatrix & matrix);

		CustomMatrix transpose();
};

