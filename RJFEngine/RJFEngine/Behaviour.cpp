#include "pch.h"
#include "Behaviour.h"

Behaviour::Behaviour() : ObjectComponent(ComponentType::BEHAVIOUR)
{
}

Behaviour::~Behaviour()
{
}

void Behaviour::OnStart()
{
}

void Behaviour::OnCollisionEnter(GameObject* other)
{
}

void Behaviour::OnCollisionExit(GameObject* other)
{
}

void Behaviour::OnCollisionStay(GameObject* other)
{
}

void Behaviour::OnUpdate() 
{
}

void Behaviour::OnRender() 
{
}

void Behaviour::OnDestroy() 
{
}