#pragma once
#include "GameVector4.h"

class GameMatrix4
{
	friend GameMatrix4 operator * (float scalar, const GameMatrix4 & matrix);
	friend GameMatrix4 operator * (const GameMatrix4 & matrix, float scalar);

	protected:
		GameVector4 row00;
		GameVector4 row01;
		GameVector4 row02;
		GameVector4 row03;

	public:
		static GameMatrix4 transpose(GameMatrix4 & matrix);

		GameMatrix4();
		GameMatrix4(const GameMatrix4 & matrix);
		GameMatrix4(GameVector4 & row00, GameVector4 & row01, GameVector4 & row02);
		GameMatrix4(GameVector4 & row00, GameVector4 & row01, GameVector4 & row02, GameVector4 & row03);
		~GameMatrix4();

		GameVector4 & operator [] (size_t index);
		GameMatrix4 operator + (const GameMatrix4 & matrix);
		GameMatrix4 operator * (const GameMatrix4 & matrix);

		void transpose();
};

