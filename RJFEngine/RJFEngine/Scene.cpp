#include "pch.h"

Scene::Scene()
{
	objects = std::make_unique<vector<GameObject *>>();
}

void Scene::initDeviceDependentResources()
{
	
}

void Scene::initWindowSizeDependentResources()
{
	GameObject * world = ResourceManager::InstantiateGameObject("World", Quaternion::Identity, Vector3::Zero, true, objects.get());
	world->AddComponent(new Primitive3D(Primitive3DType::SPHERE, 1.0f, 12));
}

float posX = 0.f;
float posY = 0.f;
float speed = 0.8f;
float rotationSpeed = XM_PI / 180.f;

float m_pitch = 0.f;
float m_yaw = 0.f;

bool Scene::update(DX::StepTimer const & timer)
{
	//if (Input::getKeyDown(Keyboard::Keys::LeftAlt)) {
	//	Input::setMouseMode(MouseMode::ABSOLUTE);
	//}
	//else {
	//	Input::setMouseMode(MouseMode::RELATIVE);
	//}

	if (Input::getKeyDown(Keyboard::Keys::Up)) {
		posY += speed * timer.GetElapsedSeconds();
	}
	if (Input::getKeyDown(Keyboard::Keys::Down)) {
		posY -= speed * timer.GetElapsedSeconds();
	}
	if (Input::getKeyDown(Keyboard::Keys::Left)) {
		posX -= speed * timer.GetElapsedSeconds();
	}
	if (Input::getKeyDown(Keyboard::Keys::Right)) {
		posX += speed * timer.GetElapsedSeconds();
	}
	//if (Input::getKeyDown(Keyboard::Keys::W)) {
	//	camera->view *= Matrix::CreateTranslation(Vector3::UnitY * -speed * timer.GetElapsedSeconds());
	//}
	//if (Input::getKeyDown(Keyboard::Keys::S)) {
	//	camera->view *= Matrix::CreateTranslation(Vector3::UnitY * speed * timer.GetElapsedSeconds());
	//}
	//if (Input::getKeyDown(Keyboard::Keys::A)) {
	//	camera->view *= Matrix::CreateTranslation(Vector3::UnitX * speed * timer.GetElapsedSeconds());
	//}
	//if (Input::getKeyDown(Keyboard::Keys::D)) {
	//	camera->view *= Matrix::CreateTranslation(Vector3::UnitX * -speed * timer.GetElapsedSeconds());
	//}

	
	//if (Input::getMouseMode() == MouseMode::ABSOLUTE)
	//{
	//	//Vector3 delta = Input::getMousePosition() * speed * timer.GetElapsedSeconds();
	//	Vector3 mousePos = Input::getMousePosition();
	//	Vector3 delta = (mousePos - Vector3(posX, posY, 0.f));

	//	if (delta.y != 0) {
	//		m_pitch = (delta.y > 0 ? rotationSpeed : -rotationSpeed);
	//	}
	//	else {
	//		m_pitch = 0.f;
	//	}

	//	if (delta.x != 0) {
	//		m_yaw = (delta.x > 0 ? rotationSpeed : -rotationSpeed);
	//	}
	//	else {
	//		m_yaw = 0.f;
	//	}

	//	//if (mousePos.x != posX || mousePos.y != posY) {
	//	//	Vector3 delta = (mousePos - Vector3(posX, posY, 0.f)) * rotationSpeed * timer.GetElapsedSeconds();

	//	//	m_pitch -= delta.y;
	//	//	m_yaw -= delta.x;

	//	//	// limit pitch to straight up or straight down
	//	//	// with a little fudge-factor to avoid gimbal lock
	//	//	float limit = XM_PI / 2.0f - 0.01f;
	//	//	m_pitch = std::max(-limit, m_pitch);
	//	//	m_pitch = std::min(+limit, m_pitch);

	//	//	// keep longitude in sane range by wrapping
	//	//	if (m_yaw > XM_PI)
	//	//	{
	//	//		m_yaw -= XM_PI * 2.0f;
	//	//	}
	//	//	else if (m_yaw < -XM_PI)
	//	//	{
	//	//		m_yaw += XM_PI * 2.0f;
	//	//	}

	//	//}

	//	camera->view *= Matrix::CreateFromYawPitchRoll(m_yaw, 0.f, 0.f);
	//	posX = mousePos.x;
	//	posY = mousePos.y;
	//}
	//
	////camera->view *= Matrix::CreateFromYawPitchRoll(mouseY - mousePos.y * speed * timer.GetElapsedSeconds(), mouseX - mousePos.x * speed * timer.GetElapsedSeconds(), 0.f);

	return true;
}

Scene * Scene::render(Graphics * graphics)
{
	graphics->drawSprite(posX, posY);
	return nullptr;
}

void Scene::reset()
{
	objects.reset();
}

void Scene::setCamera(Camera * camera)
{
	this->camera = camera;
}

vector<GameObject*>* Scene::getObjectList()
{
	return objects.get();
}
