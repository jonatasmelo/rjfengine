#include "pch.h"
#include "BehaviourProcessor.h"

BehaviourProcessor::BehaviourProcessor()
{
}

BehaviourProcessor::~BehaviourProcessor()
{
}

void BehaviourProcessor::Execute(GameObject* objA)
{
	if (objA != nullptr && objA->behaviour != nullptr)
	{
		objA->behaviour->OnUpdate();
	}
}