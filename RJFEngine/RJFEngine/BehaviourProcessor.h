#pragma once

#include "Processor.h"

class BehaviourProcessor : Processor
{
public:
	BehaviourProcessor();
	~BehaviourProcessor();

	void Execute(GameObject* objA) override;
};

