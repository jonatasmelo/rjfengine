#include "pch.h"
#include "ResourceManager.h"

ResourceManager * ResourceManager::instance = nullptr;
DeviceResources * ResourceManager::deviceResources = nullptr;

ResourceManager::ResourceManager()
{
	createCommonStates();
	createEffectFactory();
	createPrimitiveFX();
	createLineFX();
}

ResourceManager::~ResourceManager()
{
	this->fxLine.reset();
	this->fxPrimitive.reset();
	this->commonStates.reset();
}

void ResourceManager::createInstance(DeviceResources * deviceResources)
{
	ResourceManager::deviceResources = deviceResources;

	if (instance != nullptr) {
		delete(instance);
	}

	instance = new ResourceManager();
}

void ResourceManager::createCommonStates() {
	commonStates = std::make_unique<CommonStates>(deviceResources->GetD3DDevice());
}

void ResourceManager::createEffectFactory() {
	fxFactory = std::make_unique<EffectFactory>(deviceResources->GetD3DDevice());
}

void ResourceManager::createPrimitiveFX() {
	EffectFactory::EffectInfo info;
	info.name = L"primitiveFX";
	info.alphaValue = 1.f;
	info.perVertexColor = true;
	info.biasedVertexNormals = false;
	info.emissiveColor = { 1.f, 1.f, 1.f};

	EffectPipelineStateDescription primitivePipelineOpaque(
		nullptr,
		CommonStates::Opaque,
		CommonStates::DepthDefault,
		CommonStates::CullNone,
		RenderTargetState(deviceResources->GetBackBufferFormat(), deviceResources->GetDepthBufferFormat())
	);

	EffectPipelineStateDescription primitivePipelineAlphaBlend(
		nullptr,
		CommonStates::AlphaBlend,
		CommonStates::DepthDefault,
		CommonStates::CullNone,
		RenderTargetState(deviceResources->GetBackBufferFormat(), deviceResources->GetDepthBufferFormat())
	);

	fxFactory->EnablePerPixelLighting(false);
	fxFactory->EnableNormalMapEffect(false);
	fxFactory->EnableFogging(false);

	fxPrimitive = fxFactory->CreateEffect(info,
		primitivePipelineOpaque,
		primitivePipelineAlphaBlend,
		VertexPositionNormalColor::InputLayout);
}

void ResourceManager::createLineFX() {
	EffectFactory::EffectInfo info;
	info.name = L"lineFX";
	info.alphaValue = 1.f;
	info.perVertexColor = true;
	info.biasedVertexNormals = false;
	info.emissiveColor = { 1.f, 1.f, 1.f };

	CD3DX12_RASTERIZER_DESC rastDesc(D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_NONE, FALSE,
		D3D12_DEFAULT_DEPTH_BIAS, D3D12_DEFAULT_DEPTH_BIAS_CLAMP,
		D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS, TRUE, FALSE, TRUE,
		0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);

	EffectPipelineStateDescription linePipelineOpaque(
		nullptr,
		CommonStates::Opaque,
		CommonStates::DepthDefault,
		rastDesc,
		RenderTargetState(deviceResources->GetBackBufferFormat(), deviceResources->GetDepthBufferFormat()),
		D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE
	);

	EffectPipelineStateDescription linePipelineAlphaBlend(
		nullptr,
		CommonStates::AlphaBlend,
		CommonStates::DepthDefault,
		rastDesc,
		RenderTargetState(deviceResources->GetBackBufferFormat(), deviceResources->GetDepthBufferFormat()),
		D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE
	);

	fxFactory->EnablePerPixelLighting(false);
	fxFactory->EnableNormalMapEffect(false);
	fxFactory->EnableFogging(false);

	fxLine = fxFactory->CreateEffect(info,
		linePipelineOpaque,
		linePipelineAlphaBlend,
		VertexPositionNormalColor::InputLayout);
}

BasicEffect * ResourceManager::getFXLine()
{
	return dynamic_cast<BasicEffect *>(instance->fxLine.get());
}

BasicEffect * ResourceManager::getFXPrimitive()
{
	return dynamic_cast<BasicEffect *>(instance->fxPrimitive.get());
}

GameObject * ResourceManager::InstantiateGameObject(string name, Quaternion rotation, Vector3 position, bool staticObject, vector<GameObject *> * objects)
{
	GameObject * object = new GameObject(name);
	object->id = objects->size();
	object->transform->Translate(position);
	object->transform->Rotate(rotation);
	object->staticObject = staticObject;

	objects->push_back(object);

	return object;
}

void ResourceManager::updateView(Matrix view)
{
	getFXPrimitive()->SetView(view);
	getFXLine()->SetView(view);
}

void ResourceManager::updateProjection(Matrix projection)
{
	getFXPrimitive()->SetProjection(projection);
	getFXLine()->SetProjection(projection);
}
