#include "pch.h"
#include "RigidBodyProcessor.h"

RigidBodyProcessor::RigidBodyProcessor()
{
}

RigidBodyProcessor::~RigidBodyProcessor()
{
}

void RigidBodyProcessor::Execute(GameObject* objA)
{
	if (objA != nullptr && !objA->staticObject && objA->rigidBody != nullptr && objA->rigidBody->reactsWithGravity && !objA->rigidBody->isKinematic)
	{
		Physics::ApplyGravityForce(objA);
	}
}