#pragma once
class Processor abstract
{
public:
	Processor();
	~Processor();

	virtual void Execute(GameObject* objA) = 0;
};