#include "pch.h"
#include "PrimitiveShape.h"

PrimitiveShape::PrimitiveShape()
{
	vectors = CustomMatrix(1, Dimensions::FOUR_DIMENSIONS);
	//transformations = MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS);
	radius = 0.f;
	color = Colors::White;
}

PrimitiveShape::~PrimitiveShape()
{
}

void PrimitiveShape::setVector(int vectorIndex, vector<float> coordinates)
{
	vectors[vectorIndex] = coordinates;
}

CustomMatrix PrimitiveShape::getVectors()
{
	return vectors;
}

float PrimitiveShape::getRadius()
{
	return radius;
}

void PrimitiveShape::setRadius(float radius)
{

	this->radius = radius;
}

CustomMatrix PrimitiveShape::getTransformations()
{
	return transformations;
}

void PrimitiveShape::setTransformations(CustomMatrix transformations)
{
	this->transformations = transformations;
}

XMVECTORF32 PrimitiveShape::getColor()
{
	return color;
}

void PrimitiveShape::setColor(XMVECTORF32 color)
{
	this->color = color;
}

void PrimitiveShape::translate(float translationX, float translationY, float translationZ, bool resetObjBeforeTransformation) {
	//CustomMatrix translation = MatrixUtils::getTranslationMatrix(translationX, translationY, translationZ);
	//transformations = (resetObjBeforeTransformation ? MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS) : transformations) * translation;
}

void PrimitiveShape::scale(float translationX, float translationY, float translationZ, bool resetObjBeforeTransformation) {
	float centerX, centerY;
	calculateCenter(centerX, centerY, resetObjBeforeTransformation);

	translate(-1 * centerX, -1 * centerY, 0, resetObjBeforeTransformation);

	//CustomMatrix scale = MatrixUtils::getScaleMatrix(translationX, translationY, translationZ);
	//transformations = (resetObjBeforeTransformation ? MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS) : transformations) * scale;

	translate(centerX, centerY, 0, false);
}

void PrimitiveShape::reflect(bool reflectionX, bool reflectionY, bool reflectionZ, bool resetObjBeforeTransformation) {
	//CustomMatrix reflection = MatrixUtils::getReflectionMatrix(reflectionX, reflectionY, reflectionZ);
	//transformations = (resetObjBeforeTransformation ? MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS) : transformations) * reflection;
}

void PrimitiveShape::rotate(float degrees, Axis axis, bool resetObjBeforeTransformation) {
	float centerX, centerY;
	calculateCenter(centerX, centerY, resetObjBeforeTransformation);

	translate(-1 * centerX, -1 * centerY, 0, resetObjBeforeTransformation);

	//CustomMatrix rotation = MatrixUtils::getRotationMatrix(degrees, axis);
	//transformations = (resetObjBeforeTransformation ? MatrixUtils::getIdentityMatrix(Dimensions::FOUR_DIMENSIONS) : transformations) * rotation;

	translate(centerX, centerY, 0, false);
}