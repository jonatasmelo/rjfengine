#pragma once

class Physics 
{
private: 
	static Physics* instance;
	Physics();

public:
	static const float gravity;	
	
	static Physics* GetInstance();
	static void ApplyGravityForce(GameObject* obj);
};